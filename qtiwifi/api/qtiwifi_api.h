// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear


#pragma once

/**
 * Create qtiwifi AIDL instance for RPC
 *
 */
void qtiWifiRpc();

void qtiWifiRpcDeinit();
