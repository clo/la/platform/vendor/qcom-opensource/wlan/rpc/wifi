/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <unistd.h>
#include <string>
#include <map>
#include <utility>
#include <someip_client.h>

#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IQtiWifi.h>
#include "qtiwifi_rpc.h"
#include "qtiwifi_service_someip_def.h"
#include "common_util.h"
#include "qti_wifi_msg.h"

#include <aidl/vendor/qti/hardware/wifi/qtiwifi/QtiWifiStatusCode.h>
#include <android/binder_interface_utils.h>

using std::string;
using std::vector;
using aidl::vendor::qti::hardware::wifi::qtiwifi::QtiWifiRpc;
using aidl::vendor::qti::hardware::wifi::qtiwifi::QtiWifiStatusCode;


inline ndk::ScopedAStatus QtiWifiCreateStatus(int32_t code,
        const char* description = NULL)
{
    //Success case
    if (!code)
        return ndk::ScopedAStatus::ok();

    //Fail case
    if (description)
        return ndk::ScopedAStatus::fromServiceSpecificErrorWithMessage(
                code, description);
    else
        return ndk::ScopedAStatus::fromServiceSpecificError(code);
}

inline ndk::ScopedAStatus QtiWifiCreateStatus(QtiWifiStatusCode code,
        const char* description = NULL)
{
    return QtiWifiCreateStatus(static_cast<int32_t>(code), description);
}

template <typename ObjT, typename WorkFuncT, typename... Args>
ndk::ScopedAStatus ValidateAndCall(
        ObjT* obj, QtiWifiStatusCode status_code_if_invalid,
        WorkFuncT&& work, Args&&... args)
{
    if (obj->isValid()) {
        return (obj->*work)(std::forward<Args>(args)...);
    } else {
        return QtiWifiCreateStatus(status_code_if_invalid);
    }
}

template <typename ObjT, typename WorkFuncT, typename ReturnT, typename... Args>
ndk::ScopedAStatus ValidateAndCall(
        ObjT* obj, QtiWifiStatusCode status_code_if_invalid,
        WorkFuncT&& work, ReturnT* ret_val, Args&&... args)
{
    if (obj->isValid()) {
        auto call_pair = (obj->*work)(std::forward<Args>(args)...);
        *ret_val = call_pair.first;
        return std::forward<ndk::ScopedAStatus>(call_pair.second);
    } else {
        return QtiWifiCreateStatus(status_code_if_invalid);
    }
}

ndk::ScopedAStatus WifiParam2NdkStatus(HalStatusParam& param);

void qtiWifiRpc();

void qtiWifiRpcDeinit();

ndk::ScopedAStatus SomeipMsgTransportWithReplyStatus(uint16_t type, vector<uint8_t>& payload);

std::shared_ptr<qti::hal::rpc::SomeipMessage> SomeipMsgTransportWithReplyMsg(uint16_t type, vector<uint8_t>& payload);
