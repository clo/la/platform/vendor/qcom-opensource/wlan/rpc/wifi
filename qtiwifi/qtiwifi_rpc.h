/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef QTIWIFI_RPC_H_
#define QTIWIFI_RPC_H_


#include <aidl/vendor/qti/hardware/wifi/qtiwifi/BnQtiWifi.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IQtiWifi.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IQtiWifiCallback.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IfaceInfo.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IfaceType.h>
#include <android-base/macros.h>
#include <utils/Looper.h>
#include <qti_wifi_msg.h>
#include <qti_wifi_message_def.h>

#include "aidl_callback_util.h"

namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace qtiwifi {

using android::hardware::wifi::aidl_callback_util::AidlCallbackHandler;

/**
 * Root AIDL interface object used to control the Wifi HAL.
 */
class QtiWifiRpc : public BnQtiWifi {
  public:
    QtiWifiRpc();

    ~QtiWifiRpc();

    bool isValid();

    // AIDL methods exposed.
    ::ndk::ScopedAStatus listAvailableInterfaces(
        std::vector<IfaceInfo>* _aidl_return) override;
    ::ndk::ScopedAStatus registerQtiWifiCallback(
        const std::shared_ptr<IQtiWifiCallback>& callback) override;
    ::ndk::ScopedAStatus doQtiWifiCmd(
        const std::string& iface, const std::string& cmd, std::string* _aidl_return) override;

    static std::shared_ptr<BnQtiWifi> createQtiWifiRpc();

    std::set<std::shared_ptr<IQtiWifiCallback>> getEventCallbacks();

  private:
    enum class RunState { STOPPED, STARTED, STOPPING };

    // Corresponding worker functions for the AIDL methods.

    ::ndk::ScopedAStatus registerQtiWifiCallbackInternal(
        const std::shared_ptr<IQtiWifiCallback>& callback);
    std::pair<std::vector<IfaceInfo>, ndk::ScopedAStatus> listAvailableInterfacesInternal();
    std::pair<std::string, ndk::ScopedAStatus> doQtiWifiCmdInternal(const std::string& iface, const std::string& cmd);

    AidlCallbackHandler<IQtiWifiCallback> event_cb_handler_;

    DISALLOW_COPY_AND_ASSIGN(QtiWifiRpc);
};

}  // namespace qtiwifi
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl

#endif  // QTIWIFI_RPC_H_
