/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "qtiwifi_api.h"
#include "qtiwifi_rpc.h"
#include "qtiwifi_rpc_common.h"
#include <android-base/logging.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/QtiWifiStatusCode.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IQtiWifiCallback.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IfaceInfo.h>
#include <aidl/vendor/qti/hardware/wifi/qtiwifi/IfaceType.h>


namespace aidl {
namespace vendor {
namespace qti {
namespace hardware {
namespace wifi {
namespace qtiwifi {

QtiWifiRpc::QtiWifiRpc(){
}

QtiWifiRpc::~QtiWifiRpc() {
    qtiWifiRpcDeinit();
}

bool QtiWifiRpc::isValid() {
    // This object is always valid.
    return true;
}

::ndk::ScopedAStatus QtiWifiRpc::registerQtiWifiCallback(
        const std::shared_ptr<IQtiWifiCallback>& in_callback) {
    return ValidateAndCall(this, QtiWifiStatusCode::FAILURE,
                           &QtiWifiRpc::registerQtiWifiCallbackInternal, in_callback);
}

::ndk::ScopedAStatus QtiWifiRpc::listAvailableInterfaces(
        std::vector<IfaceInfo>* _aidl_return)
{
    return ValidateAndCall(this, QtiWifiStatusCode::FAILURE,
                           &QtiWifiRpc::listAvailableInterfacesInternal, _aidl_return);
}

::ndk::ScopedAStatus QtiWifiRpc::doQtiWifiCmd(
        const std::string& iface, const std::string& cmd, std::string* _aidl_return)
{
    return ValidateAndCall(this, QtiWifiStatusCode::FAILURE,
                           &QtiWifiRpc::doQtiWifiCmdInternal, _aidl_return, iface, cmd);
}

std::set<std::shared_ptr<IQtiWifiCallback>> QtiWifiRpc::getEventCallbacks()
{
    return event_cb_handler_.getCallbacks();
}

::ndk::ScopedAStatus QtiWifiRpc::registerQtiWifiCallbackInternal(
        const std::shared_ptr<IQtiWifiCallback>& callback) {
    if (!event_cb_handler_.addCallback(callback)) {
        return QtiWifiCreateStatus(QtiWifiStatusCode::FAILURE);
    }
    return ndk::ScopedAStatus::ok();
}

std::pair<std::vector<IfaceInfo>, ndk::ScopedAStatus>
QtiWifiRpc::listAvailableInterfacesInternal()
{
    std::vector<uint8_t>payload;
    ListAvailableInterfacesCfmParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(QTI_WIFI_LIST_AVAILABLE_INTERFACES_REQ, payload);
    if (rsp && QtiWifiParseListAvailableInterfacesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};
    return {std::vector<IfaceInfo>{}, QtiWifiCreateStatus(QtiWifiStatusCode::FAILURE)};
}

std::pair<std::string, ndk::ScopedAStatus>
QtiWifiRpc::doQtiWifiCmdInternal(const std::string& iface_name, const std::string& cmd)
{
    std::vector<uint8_t> payload;
    DoQtiWifiCmdCfmParam param;
    if (QtiWifiSerializeDoQtiWifiCmdReq(iface_name, cmd, payload)) {
        auto rsp = SomeipMsgTransportWithReplyMsg(QTI_WIFI_DO_QTI_WIFI_CMD_REQ, payload);
        if (rsp && QtiWifiParseDoQtiWifiCmdCfm(rsp->getData(), rsp->getLength(), param))
            return {param.result, WifiParam2NdkStatus(param.status)};
    }
    return {"", QtiWifiCreateStatus(QtiWifiStatusCode::FAILURE)};
}

}  // namespace qtiwifi
}  // namespace wifi
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
}  // namespace aidl

