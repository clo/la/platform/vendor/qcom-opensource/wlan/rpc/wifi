/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <aidl/vendor/qti/hardware/wifi/qtiwifi/QtiWifiStatusCode.h>
#include <android-base/logging.h>
#include <someip_common_def.h>
#include <utils/Log.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <android/binder_process.h>
#include <android/binder_manager.h>

#include "qtiwifi_rpc_common.h"
#include "qtiwifi_service_someip_def.h"

using aidl::vendor::qti::hardware::wifi::qtiwifi::QtiWifiStatusCode;
using qti::hal::rpc::Someip;
using qti::hal::rpc::SomeipCallback;
using qti::hal::rpc::SomeipClient;
using qti::hal::rpc::SomeipContext;
using qti::hal::rpc::SomeipMessage;

typedef std::function<void(uint8_t*, size_t)> EventDataHandler;

static std::shared_ptr<SomeipClient> client;
static std::shared_ptr<QtiWifiRpc> iQtiWifi;
static std::vector<uint16_t> events;
static std::map<uint16_t, EventDataHandler> event_handler_map;
static uint16_t instance_id;

static bool isEnableVerboseLog = false;

static void print_someip_info(uint16_t methodID,  uint8_t* data,  size_t length)
{
    ALOGD("Send SOME/IP message methodID: 0x%0x, payload length: %d", methodID, length);
}

static void handleQtiWifiOnCtrlEvent(uint8_t *data, size_t length) {
    OnCtrlEventIndParam param;
    if (QtiWifiParseOnCtrlEventInd(data, length, param)) {
        for (const auto& callback : iQtiWifi->getEventCallbacks()) {
             if (!callback->onCtrlEvent(param.iface, param.event).isOk())
                 ALOGE("Failed to invoke onCtrlEvent callback");
        }
    } else {
      ALOGE("%s: message parse failure", __func__);
    }
}

static void QtiWifiRpcInitEventHandler()
{
    events.push_back(QTI_WIFI_ON_CTRL_EVENT_IND);
    event_handler_map[QTI_WIFI_ON_CTRL_EVENT_IND] = handleQtiWifiOnCtrlEvent;
}

static int registerAidlService()
{
#ifdef WIFI_RPC_CEM
    std::string instance = std::string() + QtiWifiRpc::descriptor + "/cem";
#else
    std::string instance = std::string() + QtiWifiRpc::descriptor + "/default";
#endif

    if (AServiceManager_addService(
                iQtiWifi->asBinder().get(),
                instance.c_str()) != STATUS_OK)
                return 1;
    return 0;
}

static void QtiWifiRpcAvailabilityHandler(int32_t service_id, int32_t instance_id, bool available)
{
    ALOGI("QtiWifi Rpc service [%04x:%04x] %s.", service_id, instance_id, available ? "available" : "unavailable");

    if (available) {
        ALOGI("SOME/IP connection available");
    } else {
        LOG(FATAL) << "SOMEIP connection lost";
    }
}

static void QtiWifiRpcEventHandler(std::shared_ptr<SomeipMessage> event)
{
    EventDataHandler handler = nullptr;
    uint16_t methodId = event->getMethodId();
    auto item = event_handler_map.find(methodId);
    if (item != event_handler_map.end())
        handler = item->second;
    if (!handler) {
        ALOGE("Received unsupported event 0x%x", methodId);
        return;
    }

    ALOGD("Received rpc event 0x%x", methodId);

    handler(event->getData(), event->getLength());
}

static void QtiWifiRpcMessageHandler(std::shared_ptr<SomeipMessage> message)
{
    if (!message)
        return;

    if (message->isEvent())
        QtiWifiRpcEventHandler(message);
}

static std::shared_ptr<SomeipMessage> QtiWifiRpcSendRequestAndWaitReply(
    uint16_t method_id, const std::vector<uint8_t>& payload)
{
    if (!client)
        return nullptr;

    auto request = std::make_shared<SomeipMessage>(
                SomeipMessage::createRequest(WIFI_QTIWIFI_SERVICE_ID, instance_id,
                                            method_id, payload, true));

    return client->sendRequestAndWaitForReply(request);
}

ndk::ScopedAStatus WifiParam2NdkStatus(HalStatusParam& param)
{
    if (param.status == 0)
        return ndk::ScopedAStatus::ok();

    return ndk::ScopedAStatus::fromServiceSpecificErrorWithMessage(param.status,
                                                                   param.info.c_str());
}

static uint16_t getSomeipInstanceId()
{
#ifdef WIFI_RPC_CEM
        return QTIWIFI_INSTANCE_ID_CEM;
#else
        return QTIWIFI_INSTANCE_ID_DEFAULT;
#endif
}


static std::string getSomeipAppName()
{
#ifdef WIFI_RPC_CEM
        return QTIWIFI_CEM_CLIENT_APP_NAME;
#else
        return QTIWIFI_CLIENT_APP_NAME;
#endif
}

void qtiWifiRpc()
{
    iQtiWifi =  ndk::SharedRefBase::make<QtiWifiRpc>();

    instance_id = getSomeipInstanceId();

    QtiWifiRpcInitEventHandler();
    SomeipCallback callback(&QtiWifiRpcAvailabilityHandler,
                           &QtiWifiRpcMessageHandler);
    SomeipContext context(WIFI_QTIWIFI_SERVICE_ID, instance_id,
                      QTIWIFI_EVENTGROUP_ID, events);
    Someip::setup(callback);
    client = std::make_shared<SomeipClient>(getSomeipAppName(), context);
    if (registerAidlService()) {
       ALOGE("Register qtiwifi rpc aidl service failed.");
    }
    if (!client || !client->start(true)) {
        ALOGE(" QtiWifi Someip client [%s] start failed.", getSomeipAppName().c_str());
        Someip::destroy();
    }
}

void qtiWifiRpcDeinit()
{
    if (client) {
        client->stop();
        client->deinit();
        client.reset();
        Someip::destroy();
    }
    events.clear();
    event_handler_map.clear();
}

ndk::ScopedAStatus SomeipMsgTransportWithReplyStatus(uint16_t type, vector<uint8_t>& payload)
{
    if (isEnableVerboseLog)
        print_someip_info(type, payload.data(), payload.size());

    auto resp = QtiWifiRpcSendRequestAndWaitReply(type, payload);
    if (!resp)
        return QtiWifiCreateStatus(QtiWifiStatusCode::FAILURE,
            "no someip response message received");

    HalStatusParam param;
    if (!WifiParseHalStatus(resp->getData(), resp->getLength(), param))
        return QtiWifiCreateStatus(QtiWifiStatusCode::FAILURE,
            "invalid someip response status");

    return WifiParam2NdkStatus(param);
}

std::shared_ptr<SomeipMessage> SomeipMsgTransportWithReplyMsg(uint16_t type, vector<uint8_t>& payload)
{
    if (isEnableVerboseLog)
        print_someip_info(type, payload.data(), payload.size());

    auto resp = QtiWifiRpcSendRequestAndWaitReply(type, payload);
    if (!resp)
        ALOGE("no someip response message %04x received", type);

    return resp;
}
