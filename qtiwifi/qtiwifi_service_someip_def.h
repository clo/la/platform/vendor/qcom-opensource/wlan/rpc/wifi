/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include "someip_common_def.h"

#define QTIWIFI_INSTANCE_ID_DEFAULT      ((uint16_t) 0x5550)

#define QTIWIFI_INSTANCE_ID_CEM          ((uint16_t) 0x5551)

#define QTIWIFI_EVENTGROUP_ID            ((uint16_t) 0xEEE0)

#define QTIWIFI_CLIENT_APP_NAME           "qtiwifi_someip_client"

#define QTIWIFI_CEM_CLIENT_APP_NAME       "qtiwifi_cem_someip_client"
