/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <android-base/logging.h>
#include <cutils/properties.h>
#include <wifi_system/interface_tool.h>
#include <libnetdevice/libnetdevice.h>
#include <libnetdevice/vlan.h>

#include "someip_vlan_cfg.h"

#define INTERVAL_US  100000

#define WAIT_NUM_COUNT_INTERVAL 100

using ::android::wifi_system::InterfaceTool;

static std::string getEthIfName()
{
    std::array<char, PROPERTY_VALUE_MAX> buffer;
    //sometimes VLAN need to create on eth1 on some platform
    property_get("persist.vendor.wlan.vlan.on", buffer.data(), defaultEthInfNameForVlanCreation);
    return buffer.data();
}

bool setIfaceUpState(const std::string if_name, bool request_up)
{
    static std::shared_ptr<InterfaceTool> iface_tool = std::make_shared<InterfaceTool>();
    return iface_tool->SetUpState(if_name.c_str(), request_up);
}

bool waitForEthInfExists()
{
   //wait at most 100 * 100000 us = 10 seconds
   int count = WAIT_NUM_COUNT_INTERVAL;
   do {
       if (::android::netdevice::exists(getEthIfName())) {
           return true;
        }

       usleep(INTERVAL_US);

   } while(--count > 0);

   LOG(ERROR) << "ethernet interface don't exist";
   return false;
}

bool createVlanInf(const std::string ifname, uint16_t id)
{
    if (!::android::netdevice::exists(ifname)) {
        if (id > 0) {
            LOG(INFO) << "Create vlan interface " << ifname << " with vlan ID " << id;
            if (!::android::netdevice::vlan::add(getEthIfName(), ifname, id)) {
                LOG(ERROR) << "Failed to create vlan interface " << ifname << " vlan id " << id;
                return false;
            }
        } else {
            //create veth interface as instances in bridge AP mode
            LOG(INFO) << "Create veth interface " << ifname;
            if (!::android::netdevice::add(ifname, "veth")) {
                LOG(ERROR) << "Failed to create veth interface " << ifname;
                return false;
             }
        }
   }

   if (!setIfaceUpState(ifname, true))
        LOG(ERROR) << "Failed to up interface " << ifname;

    return true;
}

bool deleteVlanInf(const std::string ifname)
{
    setIfaceUpState(ifname, false);
    LOG(INFO) << "Remove interface " << ifname;
    if (::android::netdevice::exists(ifname)) {
        if (!::android::netdevice::del(ifname)) {
            LOG(ERROR) << "Failed to delete vlan interface " << ifname;
            return false;
        }
    }
    return true;
}
