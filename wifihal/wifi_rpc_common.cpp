/*
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <aidl/android/hardware/wifi/WifiStatusCode.h>
#include <android-base/logging.h>
#include <someip_common_def.h>
#include <utils/Log.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "wifi_api.h"
#include "wifi_rpc.h"
#include "wifi_rpc_common.h"
#include "someip_vlan_cfg.h"
#include "wifi_service_someip_def.h"
#include "wifi_status_util.h"

using aidl::android::hardware::wifi::createWifiStatus;
using aidl::android::hardware::wifi::WifiRpc;
using aidl::android::hardware::wifi::WifiStatusCode;
using qti::hal::rpc::Someip;
using qti::hal::rpc::SomeipCallback;
using qti::hal::rpc::SomeipClient;
using qti::hal::rpc::SomeipContext;

#define WIFI_MESSAGE_PAYLOAD_HEADER_SIZE 4

typedef std::function<void(uint8_t*, size_t)> EventDataHandler;

static std::shared_ptr<SomeipClient> client;

static std::vector<uint16_t> events;
static std::map<uint16_t, EventDataHandler> event_handler_map;

static std::shared_ptr<WifiRpc> iwifi_rpc;

static bool isEnableVerboseLog = false;

static uint16_t instance_id;

static void print_someip_info(uint16_t methodID,  uint8_t* data,  size_t length)
{
    ALOGD("Send SOME/IP message methodID: 0x%0x, payload length: %d", methodID, length);
}

template <typename ProtoFuncT, typename... Args>
static bool WifiParseEventPayload(uint8_t* data, size_t length,
    ProtoFuncT&& protoFunc, uint16_t& chipId, uint16_t& ifaceId, Args&&... args)
{
    if (!data || length < WIFI_MESSAGE_PAYLOAD_HEADER_SIZE) {
        ALOGE("rpc event: invalid payload length %d", length);
        return false;
    }

    chipId = (uint16_t)data[0] | (((uint16_t)data[1]) << 8);
    ifaceId = ((uint16_t)data[2]) | (((uint16_t)data[3]) << 8);

    return (*protoFunc)(data + WIFI_MESSAGE_PAYLOAD_HEADER_SIZE,
        length - WIFI_MESSAGE_PAYLOAD_HEADER_SIZE, std::forward<Args>(args)...);
}

static std::shared_ptr<WifiChipRpc> WifiGetChip(int32_t chipId)
{
    auto chips = iwifi_rpc->getChips();
    for (std::shared_ptr<WifiChipRpc> chip : chips) {
        int32_t chipRpcId;
        if (chip && chip->getId(&chipRpcId).isOk())
            if (chipRpcId == chipId)
                return chip;
    }

    return nullptr;
}

static std::shared_ptr<WifiStaIfaceRpc> WifiGetStaIface(int32_t chipId,
    int32_t ifaceId)
{
    auto chip = WifiGetChip(chipId);
    if (chip) {
        auto staIfaces = chip->getStaIfacesPointer();
        for (std::shared_ptr<WifiStaIfaceRpc> staIface : staIfaces) {
            if (staIface->getStaInstanceId() == ifaceId)
                return staIface;
        }
    }

    return nullptr;
}

static void handleWifiEventSubsystemRestart(uint8_t *data, size_t length)
{
    uint16_t chipId, ifaceId;
    WifiStatusCode param;
    if (WifiParseEventPayload(data, length, &WifiParseOnSubsystemRestartInd,
        chipId, ifaceId, param)) {
        for (const auto& callback : iwifi_rpc->getEventCallbacks()) {
            ALOGD("Attempting to invoke onSubsystemRestart callback");
            if (!callback->onSubsystemRestart(param).isOk())
                ALOGE("Failed to invoke onSubsystemRestart callback");
            else
                ALOGD("Succeeded to invoke onSubsystemRestart callback");
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void handleWifiChipEventRaidoModeChange(uint8_t *data, size_t length)
{
    uint16_t chipId, ifaceId;
    vector<IWifiChipEventCallback::RadioModeInfo> param;
    if (WifiParseEventPayload(data, length, &WifiChipParseOnRadioModeChangeInd,
        chipId, ifaceId, param)) {
        std::shared_ptr<WifiChipRpc> chip = WifiGetChip(chipId);
        if (chip) {
            for (const auto& callback : chip->getEventCallbacks()) {
                if (!callback->onRadioModeChange(param).isOk()) {
                    ALOGE("Failed to invoke onRadioModeChange callback");
                }
            }
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void handleWifiStaIfaceEventOnBackgroundFullScanResult(uint8_t *data,
    size_t length)
{
    uint16_t chipId, ifaceId;
    OnBackgroundFullScanResultIndStaIfaceParam param;
    if (WifiParseEventPayload(data, length,
        &WifiStaIfaceParseOnBackgroundFullScanResultInd,
        chipId, ifaceId, param)) {
        auto staIface = WifiGetStaIface(chipId, ifaceId);
        if (staIface) {
            for (const auto& callback : staIface->getEventCallbacks()) {
                if (!callback->onBackgroundFullScanResult(param.cmdId,
                    param.bucketsScanned, param.result).isOk()) {
                    ALOGE("Invoke onBackgroundFullScanResult callback fail");
                }
            }
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void handleWifiStaIfaceEventOnBackgroundScanFailure(uint8_t *data,
    size_t length)
{
    int32_t param;
    uint16_t chipId, ifaceId;
    if (WifiParseEventPayload(data, length,
        &WifiStaIfaceParseOnBackgroundScanFailureInd, chipId, ifaceId, param)) {
        auto staIface = WifiGetStaIface(chipId, ifaceId);
        if (staIface) {
            for (const auto& callback : staIface->getEventCallbacks()) {
                if (!callback->onBackgroundScanFailure(param).isOk()) {
                    ALOGE("Failed to invoke onBackgroundScanFailure callback");
                }
            }
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void handleWifiStaIfaceEventOnBackgroundScanResults(uint8_t *data,
    size_t length)
{
    uint16_t chipId, ifaceId;
    OnBackgroundScanResultsIndStaIfaceParam param;
    if (WifiParseEventPayload(data, length,
        &WifiStaIfaceParseOnBackgroundScanResultsInd, chipId, ifaceId, param)) {
        auto staIface = WifiGetStaIface(chipId, ifaceId);
        if (staIface) {
            for (const auto& callback : staIface->getEventCallbacks()) {
                if (!callback->onBackgroundScanResults(param.cmdId,
                    param.scanDatas).isOk()) {
                    ALOGE("Failed to invoke onBackgroundScanResults callback");
                }
            }
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void handleWifiStaIfaceEventOnRssiThresholdBreached(uint8_t *data,
    size_t length)
{
    uint16_t chipId, ifaceId;
    OnRssiThresholdBreachedIndStaIfaceParam param;
    if (WifiParseEventPayload(data, length,
        &WifiStaIfaceParseOnRssiThresholdBreachedInd, chipId, ifaceId, param)) {
        auto staIface = WifiGetStaIface(chipId, ifaceId);
        if (staIface) {
            for (const auto& callback : staIface->getEventCallbacks()) {
                if (!callback->onRssiThresholdBreached(param.cmdId,
                    param.currBssid, param.currRssi).isOk()) {
                    ALOGE("Failed to invoke onRssiThresholdBreached callback");
                }
            }
        }
    } else {
        ALOGE("%s: message parse failure", __func__);
    }
}

static void WifiHalRpcInitEventHandler()
{
    events.clear();
    event_handler_map.clear();

    events.push_back(WIFI_ON_SUBSYSTEM_RESTART_IND);
    event_handler_map[WIFI_ON_SUBSYSTEM_RESTART_IND] =
        &handleWifiEventSubsystemRestart;

    events.push_back(WIFI_CHIP_ON_RADIO_MODE_CHANGE_IND);
    event_handler_map[WIFI_CHIP_ON_RADIO_MODE_CHANGE_IND] =
        &handleWifiChipEventRaidoModeChange;

    events.push_back(WIFI_STA_IFACE_ON_BACKGROUND_FULL_SCAN_RESULT_IND);
    event_handler_map[WIFI_STA_IFACE_ON_BACKGROUND_FULL_SCAN_RESULT_IND] =
        &handleWifiStaIfaceEventOnBackgroundFullScanResult;

    events.push_back(WIFI_STA_IFACE_ON_BACKGROUND_SCAN_FAILURE_IND);
    event_handler_map[WIFI_STA_IFACE_ON_BACKGROUND_SCAN_FAILURE_IND] =
        &handleWifiStaIfaceEventOnBackgroundScanFailure;

    events.push_back(WIFI_STA_IFACE_ON_BACKGROUND_SCAN_RESULTS_IND);
    event_handler_map[WIFI_STA_IFACE_ON_BACKGROUND_SCAN_RESULTS_IND] =
        &handleWifiStaIfaceEventOnBackgroundScanResults;

    events.push_back(WIFI_STA_IFACE_ON_RSSI_THRESHOLD_BREACHED_IND);
    event_handler_map[WIFI_STA_IFACE_ON_RSSI_THRESHOLD_BREACHED_IND] =
        &handleWifiStaIfaceEventOnRssiThresholdBreached;
}

static void WifiHalRpcAvailabilityHandler(int32_t service_id,
    int32_t instance_id, bool available)
{
    ALOGI("RPC service [%04x:%04x] %s.", service_id, instance_id,
        available ? "available" : "unavailable");

    if (!available)
        LOG(FATAL) << "SOMEIP connection lost";
}

static void WifiHalRpcEventHandler(std::shared_ptr<SomeipMessage> event)
{
    EventDataHandler handler = nullptr;
    uint16_t methodId = event->getMethodId();
    auto item = event_handler_map.find(methodId);
    if (item != event_handler_map.end())
        handler = item->second;
    if (!handler) {
        ALOGE("Received unsupported event 0x%x", methodId);
        return;
    }

    ALOGD("Received rpc event 0x%x", methodId);

    handler(event->getData(), event->getLength());
}

static void WifiHalRpcMessageHandler(std::shared_ptr<SomeipMessage> message)
{
    if (!message)
        return;

    if (message->isEvent())
        WifiHalRpcEventHandler(message);
}

ndk::ScopedAStatus WifiParam2NdkStatus(HalStatusParam& param)
{
    if (param.status == 0)
        return ndk::ScopedAStatus::ok();

    return ndk::ScopedAStatus::fromServiceSpecificErrorWithMessage(
        param.status, param.info.c_str());
}

static uint16_t getSomeipInstanceId()
{
#ifdef WIFI_RPC_CEM
        return WIFI_INSTANCE_ID_CEM;
#else
        return WIFI_INSTANCE_ID_DEFAULT;
#endif
}

static std::string getSomeipAppName()
{
#ifdef WIFI_RPC_CEM
        return WIFI_CEM_CLIENT_APP_NAME;
#else
        return WIFI_CLIENT_APP_NAME;
#endif
}

std::shared_ptr<BnWifi> createWifiRpc()
{
    if (iwifi_rpc) {
        ALOGI("Wifi hal rpc already started");
        return iwifi_rpc;
    }

    //VLAN creation depends on ethernet interface exist
    if (waitForEthInfExists()) {
        createVlanInf(kSomeIpMsgIfName, kSomeIpMsgVlanId);
    } else {
        ALOGE("fail to create VLAN interface %s for SOME/IP message",
              kSomeIpMsgIfName.c_str());
    }

    WifiHalRpcInitEventHandler();

    instance_id = getSomeipInstanceId();

    SomeipCallback callback(&WifiHalRpcAvailabilityHandler,
        &WifiHalRpcMessageHandler);
    SomeipContext context(WIFI_HAL_SERVICE_ID, instance_id,
        WIFI_EVENTGROUP_ID, events);

    Someip::setup(callback);
    client = std::make_shared<SomeipClient>(getSomeipAppName(), context);
    if (!client || !client->start(true)) {
        ALOGE("Wifi hal someip client start fail");
        Someip::destroy();
        return nullptr;
    }

    iwifi_rpc = ndk::SharedRefBase::make<WifiRpc>();

    return iwifi_rpc;
}

void wifiRpcDeinit()
{
    if (client) {
        client->stop();
        client->deinit();
        client.reset();
        Someip::destroy();
    }

    events.clear();
    event_handler_map.clear();
}

bool isWifiSomeIPConnectionAvailable()
{
    return client ? client->isServiceAvailable() : false;
}

WifiRpcRequest::WifiRpcRequest(uint16_t chipId, uint16_t ifaceId, bool reliable)
    : chipId_(chipId), ifaceId_(ifaceId), reliable_(reliable) {}

std::shared_ptr<SomeipMessage> WifiRpcRequest::CreateRequest(uint16_t methodId,
    const std::vector<uint8_t>& data)
{
    auto request = std::make_shared<SomeipMessage>(
        SomeipMessage::createRequest(WIFI_HAL_SERVICE_ID, instance_id,
            methodId, std::vector<uint8_t>(), reliable_));
    if (!request) {
        ALOGE("Fail to create request message with method id 0x%x", methodId);
        return nullptr;
    }

    auto payload = request->createPayload(
        WIFI_MESSAGE_PAYLOAD_HEADER_SIZE + data.size());
    payload.push_back((uint8_t)(chipId_ & 0xff));
    payload.push_back((uint8_t)((chipId_ >> 8) & 0xff));
    payload.push_back((uint8_t)(ifaceId_ & 0xff));
    payload.push_back((uint8_t)((ifaceId_ >> 8) & 0xff));
    if (data.size())
        payload.insert(payload.end(), data.begin(), data.end());
    request->setPayload(payload);

    return request;
}

std::shared_ptr<SomeipMessage> WifiRpcRequest::SomeipMsgTransportWithReplyMsg(
    uint16_t method_id, const std::vector<uint8_t>& data, uint32_t timeout)
{
    if (!client)
        return nullptr;

    auto request = CreateRequest(method_id, data);
    if (!request)
        return nullptr;

    if (isEnableVerboseLog)
        print_someip_info(method_id, request->getData(), request->getLength());

    auto response = client->sendRequestAndWaitForReply(request, timeout);
    if (!response)
        ALOGE("no someip response message %04x received", request->getMethodId());

    return response;
}

ndk::ScopedAStatus WifiRpcRequest::SomeipMsgTransportWithReplyStatus(
    uint16_t method_id, const std::vector<uint8_t>& data, uint32_t timeout)
{
    auto response = SomeipMsgTransportWithReplyMsg(method_id, data, timeout);
    if (!response)
        return createWifiStatus(WifiStatusCode::ERROR_UNKNOWN,
            "no someip response message received");

    HalStatusParam param;
    if (!WifiParseHalStatus(response->getData(), response->getLength(), param))
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS,
            "invalid someip response status");

    return WifiParam2NdkStatus(param);
}
