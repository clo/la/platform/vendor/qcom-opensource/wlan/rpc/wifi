/*
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include <unistd.h>
#include <string>
#include <map>
#include <utility>
#include <someip_client.h>

#include <aidl/android/hardware/wifi/IWifi.h>
#include "wifi_msg.h"
#include "wifi_chip_msg.h"
#include "wifi_sta_iface_msg.h"
#include "wifi_ap_iface_msg.h"
#include "wifi_msg_common.h"
#include "wifi_service_someip_def.h"
#include "common_util.h"

using qti::hal::rpc::SomeipMessage;
using std::string;
using std::vector;

ndk::ScopedAStatus WifiParam2NdkStatus(HalStatusParam& param);

void wifiRpcDeinit();

class WifiRpcRequest {
public:
    WifiRpcRequest(uint16_t chipId = 0, uint16_t ifaceId = 0,
        bool reliable = true);
    ~WifiRpcRequest() = default;

    std::shared_ptr<SomeipMessage> CreateRequest(uint16_t methodId,
        const std::vector<uint8_t>& data = {});
    std::shared_ptr<SomeipMessage> SomeipMsgTransportWithReplyMsg(
        uint16_t method_id, const std::vector<uint8_t>& data = {},
        uint32_t timeout = 500);
    ndk::ScopedAStatus SomeipMsgTransportWithReplyStatus(uint16_t method_id,
        const std::vector<uint8_t>& data = {}, uint32_t timeout = 500);

private:
    bool reliable_;
    uint16_t chipId_;
    uint16_t ifaceId_;
};
