/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include <string>

const char defaultEthInfNameForVlanCreation[] = "eth0";
const std::string kSomeIpMsgIfName = "vlan40";
constexpr uint16_t kSomeIpMsgVlanId = 40;
constexpr uint16_t kPrimaryStaVlanId = 41;
constexpr uint16_t kSecondaryStaVlanId = 42;
constexpr uint16_t kChmBrApVlanId = 43;
constexpr uint16_t kCemBrApVlanId = 47;

bool waitForEthInfExists();

bool createVlanInf(const std::string ifname, uint16_t id = 0);

bool deleteVlanInf(std::string ifname);

