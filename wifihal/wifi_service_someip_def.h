/*
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include "wifi_message_def.h"

#include "someip_common_def.h"

#define WIFI_INSTANCE_ID_DEFAULT      ((uint16_t) 0x1110)

#define WIFI_INSTANCE_ID_CEM      ((uint16_t) 0x1111)

#define WIFI_EVENTGROUP_ID            ((uint16_t) 0xAAA0)

#define WIFI_CLIENT_APP_NAME           "wifi_hal_client"

#define WIFI_CEM_CLIENT_APP_NAME       "wifi_cem_hal_client"
