/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "wifi_ap_iface_rpc.h"

#include <android-base/logging.h>

#include "aidl_return_util.h"
#include "aidl_struct_util.h"
#include "wifi_status_util.h"

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
using aidl_return_util::validateAndCall;

WifiApIfaceRpc::WifiApIfaceRpc(const std::string& ifname,
    const std::vector<std::string>& instances, int32_t chipId, int32_t apInstanceId)
    : WifiRpcRequest(chipId, apInstanceId),
      ifname_(ifname),
      instances_(instances),
      is_valid_(true) {
}

void WifiApIfaceRpc::invalidate() {
    is_valid_ = false;
}

bool WifiApIfaceRpc::isValid() {
    return is_valid_;
}

std::string WifiApIfaceRpc::getName() {
    return ifname_;
}

void WifiApIfaceRpc::removeInstance(std::string instance) {
    instances_.erase(std::remove(instances_.begin(), instances_.end(), instance), instances_.end());
}

ndk::ScopedAStatus WifiApIfaceRpc::getName(std::string* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::getNameInternal, _aidl_return);
}

ndk::ScopedAStatus WifiApIfaceRpc::setCountryCode(const std::array<uint8_t, 2>& in_code) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::setCountryCodeInternal, in_code);
}

ndk::ScopedAStatus WifiApIfaceRpc::setMacAddress(const std::array<uint8_t, 6>& in_mac) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::setMacAddressInternal, in_mac);
}

ndk::ScopedAStatus WifiApIfaceRpc::getFactoryMacAddress(std::array<uint8_t, 6>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::getFactoryMacAddressInternal, _aidl_return,
                           instances_.size() > 0 ? instances_[0] : ifname_);
}

ndk::ScopedAStatus WifiApIfaceRpc::resetToFactoryMacAddress() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::resetToFactoryMacAddressInternal);
}

ndk::ScopedAStatus WifiApIfaceRpc::getBridgedInstances(std::vector<std::string>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiApIfaceRpc::getBridgedInstancesInternal, _aidl_return);
}

std::pair<std::string, ndk::ScopedAStatus> WifiApIfaceRpc::getNameInternal() {
    return {ifname_, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiApIfaceRpc::setCountryCodeInternal(const std::array<uint8_t, 2>& code) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiApIfaceSerializeSetCountryCodeReq(code, payload)) {
	status = SomeipMsgTransportWithReplyStatus(WIFI_AP_IFACE_SET_COUNTRY_CODE_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiApIfaceRpc::setMacAddressInternal(const std::array<uint8_t, 6>& mac) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiApIfaceSerializeSetMacAddressReq(mac, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_AP_IFACE_SET_MAC_ADDRESS_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

std::pair<std::array<uint8_t, 6>, ndk::ScopedAStatus> WifiApIfaceRpc::getFactoryMacAddressInternal(
        const std::string& ifaceName) {
    ndk::ScopedAStatus status;
    GetFactoryMacAddressCfmApIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_AP_IFACE_GET_FACTORY_MAC_ADDRESS_REQ);
    if (rsp && WifiApIfaceParseGetFactoryMacAddressCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::array<uint8_t, 6>(), createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiApIfaceRpc::resetToFactoryMacAddressInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_AP_IFACE_RESET_TO_FACTORY_MAC_ADDRESS_REQ);
}

std::pair<std::vector<std::string>, ndk::ScopedAStatus> WifiApIfaceRpc::getBridgedInstancesInternal() {
    GetBridgedInstancesCfmApIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_AP_IFACE_GET_BRIDGED_INSTANCES_REQ);
    if (rsp && WifiApIfaceParseGetBridgedInstancesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};
    return {std::vector<std::string>(), createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
