/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "wifi_sta_iface_rpc.h"

#include <android-base/logging.h>

#include "aidl_return_util.h"
#include "aidl_struct_util.h"
#include "wifi_status_util.h"

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
using aidl_return_util::validateAndCall;

WifiStaIfaceRpc::WifiStaIfaceRpc(const std::string& ifname, int32_t chipId, int32_t staInstanceId)
    : WifiRpcRequest(chipId, staInstanceId), ifname_(ifname), staInstanceId_(staInstanceId), is_valid_(true) {
}

std::shared_ptr<WifiStaIfaceRpc> WifiStaIfaceRpc::create(
        const std::string& ifname, int32_t chipId, int32_t staInstanceId) {
    std::shared_ptr<WifiStaIfaceRpc> ptr =
            ndk::SharedRefBase::make<WifiStaIfaceRpc>(ifname, chipId, staInstanceId);
    std::weak_ptr<WifiStaIfaceRpc> weak_ptr_this(ptr);
    ptr->setWeakPtr(weak_ptr_this);
    return ptr;
}

void WifiStaIfaceRpc::invalidate() {
    event_cb_handler_.invalidate();
    is_valid_ = false;
}

void WifiStaIfaceRpc::setWeakPtr(std::weak_ptr<WifiStaIfaceRpc> ptr) {
    weak_ptr_this_ = ptr;
}

bool WifiStaIfaceRpc::isValid() {
    return is_valid_;
}

std::string WifiStaIfaceRpc::getName() {
    return ifname_;
}

int32_t WifiStaIfaceRpc::getStaInstanceId() {
	return staInstanceId_;
}

std::set<std::shared_ptr<IWifiStaIfaceEventCallback>> WifiStaIfaceRpc::getEventCallbacks() {
    return event_cb_handler_.getCallbacks();
}

ndk::ScopedAStatus WifiStaIfaceRpc::getName(std::string* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getNameInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::registerEventCallback(
        const std::shared_ptr<IWifiStaIfaceEventCallback>& in_callback) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::registerEventCallbackInternal, in_callback);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getFeatureSet(int32_t* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getFeatureSetInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getApfPacketFilterCapabilities(
        StaApfPacketFilterCapabilities* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getApfPacketFilterCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::installApfPacketFilter(const std::vector<uint8_t>& in_program) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::installApfPacketFilterInternal, in_program);
}

ndk::ScopedAStatus WifiStaIfaceRpc::readApfPacketFilterData(std::vector<uint8_t>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::readApfPacketFilterDataInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getBackgroundScanCapabilities(
        StaBackgroundScanCapabilities* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getBackgroundScanCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::startBackgroundScan(int32_t in_cmdId,
                                                     const StaBackgroundScanParameters& in_params) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::startBackgroundScanInternal, in_cmdId, in_params);
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopBackgroundScan(int32_t in_cmdId) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::stopBackgroundScanInternal, in_cmdId);
}

ndk::ScopedAStatus WifiStaIfaceRpc::enableLinkLayerStatsCollection(bool in_debug) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::enableLinkLayerStatsCollectionInternal, in_debug);
}

ndk::ScopedAStatus WifiStaIfaceRpc::disableLinkLayerStatsCollection() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::disableLinkLayerStatsCollectionInternal);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getLinkLayerStats(StaLinkLayerStats* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getLinkLayerStatsInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::startRssiMonitoring(int32_t in_cmdId, int32_t in_maxRssi,
                                                     int32_t in_minRssi) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::startRssiMonitoringInternal, in_cmdId, in_maxRssi,
                           in_minRssi);
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopRssiMonitoring(int32_t in_cmdId) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::stopRssiMonitoringInternal, in_cmdId);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getRoamingCapabilities(StaRoamingCapabilities* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getRoamingCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::configureRoaming(const StaRoamingConfig& in_config) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::configureRoamingInternal, in_config);
}

ndk::ScopedAStatus WifiStaIfaceRpc::setRoamingState(StaRoamingState in_state) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::setRoamingStateInternal, in_state);
}

ndk::ScopedAStatus WifiStaIfaceRpc::enableNdOffload(bool in_enable) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::enableNdOffloadInternal, in_enable);
}

ndk::ScopedAStatus WifiStaIfaceRpc::startSendingKeepAlivePackets(
        int32_t in_cmdId, const std::vector<uint8_t>& in_ipPacketData, char16_t in_etherType,
        const std::array<uint8_t, 6>& in_srcAddress, const std::array<uint8_t, 6>& in_dstAddress,
        int32_t in_periodInMs) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::startSendingKeepAlivePacketsInternal, in_cmdId,
                           in_ipPacketData, in_etherType, in_srcAddress, in_dstAddress,
                           in_periodInMs);
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopSendingKeepAlivePackets(int32_t in_cmdId) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::stopSendingKeepAlivePacketsInternal, in_cmdId);
}

ndk::ScopedAStatus WifiStaIfaceRpc::startDebugPacketFateMonitoring() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::startDebugPacketFateMonitoringInternal);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getDebugTxPacketFates(
        std::vector<WifiDebugTxPacketFateReport>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getDebugTxPacketFatesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getDebugRxPacketFates(
        std::vector<WifiDebugRxPacketFateReport>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getDebugRxPacketFatesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::setMacAddress(const std::array<uint8_t, 6>& in_mac) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::setMacAddressInternal, in_mac);
}

ndk::ScopedAStatus WifiStaIfaceRpc::getFactoryMacAddress(std::array<uint8_t, 6>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::getFactoryMacAddressInternal, _aidl_return);
}

ndk::ScopedAStatus WifiStaIfaceRpc::setScanMode(bool in_enable) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::setScanModeInternal, in_enable);
}

ndk::ScopedAStatus WifiStaIfaceRpc::setDtimMultiplier(int32_t in_multiplier) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiStaIfaceRpc::setDtimMultiplierInternal, in_multiplier);
}

std::pair<std::string, ndk::ScopedAStatus> WifiStaIfaceRpc::getNameInternal() {
    return {ifname_, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiStaIfaceRpc::registerEventCallbackInternal(
        const std::shared_ptr<IWifiStaIfaceEventCallback>& callback) {
    if (!event_cb_handler_.addCallback(callback)) {
        return createWifiStatus(WifiStatusCode::ERROR_UNKNOWN);
    }
    return ndk::ScopedAStatus::ok();
}

std::pair<int32_t, ndk::ScopedAStatus> WifiStaIfaceRpc::getFeatureSetInternal() {
    GetFeatureSetCfmStaIfaceParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_FEATURE_SET_REQ);
    if (rsp && WifiStaIfaceParseGetFeatureSetCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {0, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

std::pair<StaApfPacketFilterCapabilities, ndk::ScopedAStatus>
WifiStaIfaceRpc::getApfPacketFilterCapabilitiesInternal() {
    GetApfPacketFilterCapabilitiesCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_APF_PACKET_FILTER_CAPABILITIES_REQ);
    if (rsp && WifiStaIfaceParseGetApfPacketFilterCapabilitiesCfm(rsp->getData(), rsp->getLength(), param)) {
        return {param.result, WifiParam2NdkStatus(param.status)};
    }

    return {StaApfPacketFilterCapabilities{}, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiStaIfaceRpc::installApfPacketFilterInternal(
        const std::vector<uint8_t>& program) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeInstallApfPacketFilterReq(program, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_INSTALL_APF_PACKET_FILTER_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus>
WifiStaIfaceRpc::readApfPacketFilterDataInternal() {
    ReadApfPacketFilterDataCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_READ_APF_PACKET_FILTER_DATA_REQ);
    if (rsp && WifiStaIfaceParseReadApfPacketFilterDataCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<uint8_t>(),  createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

std::pair<StaBackgroundScanCapabilities, ndk::ScopedAStatus>
WifiStaIfaceRpc::getBackgroundScanCapabilitiesInternal() {
    GetBackgroundScanCapabilitiesCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_BACKGROUND_SCAN_CAPABILITIES_REQ);
    if (rsp && WifiStaIfaceParseGetBackgroundScanCapabilitiesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {StaBackgroundScanCapabilities{}, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiStaIfaceRpc::startBackgroundScanInternal(
        int32_t cmd_id, const StaBackgroundScanParameters& params) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeStartBackgroundScanReq(cmd_id, params, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_START_BACKGROUND_SCAN_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopBackgroundScanInternal(int32_t cmd_id) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiStaIfaceSerializeStopBackgroundScanReq(cmd_id, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_STOP_BACKGROUND_SCAN_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::enableLinkLayerStatsCollectionInternal(bool debug) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeEnableLinkLayerStatsCollectionReq(debug, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_ENABLE_LINK_LAYER_STATS_COLLECTION_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }
    return status;

}

ndk::ScopedAStatus WifiStaIfaceRpc::disableLinkLayerStatsCollectionInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_DISABLE_LINK_LAYER_STATS_COLLECTION_REQ);
}

std::pair<StaLinkLayerStats, ndk::ScopedAStatus> WifiStaIfaceRpc::getLinkLayerStatsInternal() {
    GetLinkLayerStatsCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_LINK_LAYER_STATS_REQ);
    if (rsp && WifiStaIfaceParseGetLinkLayerStatsCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {StaLinkLayerStats{}, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};

}

ndk::ScopedAStatus WifiStaIfaceRpc::startRssiMonitoringInternal(int32_t cmd_id,
    int32_t max_rssi, int32_t min_rssi) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeStartRssiMonitoringReq(cmd_id, max_rssi, min_rssi, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_START_RSSI_MONITORING_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopRssiMonitoringInternal(int32_t cmd_id) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiStaIfaceSerializeStopRssiMonitoringReq(cmd_id, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_STOP_RSSI_MONITORING_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }
    return status;
}

std::pair<StaRoamingCapabilities, ndk::ScopedAStatus>
WifiStaIfaceRpc::getRoamingCapabilitiesInternal() {
    GetRoamingCapabilitiesCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_ROAMING_CAPABILITIES_REQ);
    if (rsp && WifiStaIfaceParseGetRoamingCapabilitiesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {StaRoamingCapabilities{}, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiStaIfaceRpc::configureRoamingInternal(const StaRoamingConfig& config) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiStaIfaceSerializeConfigureRoamingReq(config, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_CONFIGURE_ROAMING_REQ, payload);
    } else {
         status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }
    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::setRoamingStateInternal(StaRoamingState state) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiStaIfaceSerializeSetRoamingStateReq(state, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_SET_ROAMING_STATE_REQ, payload);
    } else {
         status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::enableNdOffloadInternal(bool enable) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeEnableNdOffloadReq(enable, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_ENABLE_ND_OFFLOAD_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::startSendingKeepAlivePacketsInternal(
    int32_t cmd_id, const std::vector<uint8_t>& ip_packet_data,
    char16_t ether_type, const std::array<uint8_t, 6>& src_address,
    const std::array<uint8_t, 6>& dst_address, int32_t period_in_ms) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeStartSendingKeepAlivePacketsReq(cmd_id,
        ip_packet_data, ether_type, src_address,dst_address, period_in_ms, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_START_SENDING_KEEP_ALIVE_PACKETS_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::stopSendingKeepAlivePacketsInternal(int32_t cmd_id) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiStaIfaceSerializeStopSendingKeepAlivePacketsReq(cmd_id, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_STOP_SENDING_KEEP_ALIVE_PACKETS_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::startDebugPacketFateMonitoringInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_START_DEBUG_PACKET_FATE_MONITORING_REQ);
}

std::pair<std::vector<WifiDebugTxPacketFateReport>, ndk::ScopedAStatus>
WifiStaIfaceRpc::getDebugTxPacketFatesInternal() {
    GetDebugTxPacketFatesCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_DEBUG_TX_PACKET_FATES_REQ);
    if (rsp && WifiStaIfaceParseGetDebugTxPacketFatesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<WifiDebugTxPacketFateReport>(),
            createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

std::pair<std::vector<WifiDebugRxPacketFateReport>, ndk::ScopedAStatus>
WifiStaIfaceRpc::getDebugRxPacketFatesInternal() {
    GetDebugRxPacketFatesCfmStaIfaceParam param;

    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_DEBUG_RX_PACKET_FATES_REQ);
    if (rsp && WifiStaIfaceParseGetDebugRxPacketFatesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<WifiDebugRxPacketFateReport>(),
            createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiStaIfaceRpc::setMacAddressInternal(const std::array<uint8_t, 6>& mac) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeSetMacAddressReq(mac, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_SET_MAC_ADDRESS_REQ, payload);
    } else {
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

std::pair<std::array<uint8_t, 6>, ndk::ScopedAStatus> WifiStaIfaceRpc::getFactoryMacAddressInternal() {
    GetFactoryMacAddressCfmStaIfaceParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_FACTORY_MAC_ADDRESS_REQ);
    if (rsp && WifiStaIfaceParseGetFactoryMacAddressCfm(rsp->getData(), rsp->getLength(), param)) {
        return {param.result, WifiParam2NdkStatus(param.status)};
    }

    return {std::array<uint8_t, 6>(), createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiStaIfaceRpc::setScanModeInternal(bool enable) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeSetScanModeReq(enable, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_SET_SCAN_MODE_REQ, payload);
    } else {
         status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

ndk::ScopedAStatus WifiStaIfaceRpc::setDtimMultiplierInternal(const int multiplier) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiStaIfaceSerializeSetDtimMultiplierReq(multiplier, payload)) {
        status = SomeipMsgTransportWithReplyStatus(WIFI_STA_IFACE_SET_DTIM_MULTIPLIER_REQ, payload);
    } else {
         status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    }

    return status;
}

}  // namespace wifi
}  // namespace hardware
}  // namespace android
}
