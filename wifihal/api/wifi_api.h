// Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear


#pragma once

#include <aidl/android/hardware/wifi/BnWifi.h>

using aidl::android::hardware::wifi::BnWifi;

/**
 * Create wifi AIDL instance for RPC
 *
 *   interfaces: wifi interface
 *
 *   return: wifi AIDL instance for RPC
 */
std::shared_ptr<BnWifi> createWifiRpc();

bool isWifiSomeIPConnectionAvailable();
