/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "wifi_rpc.h"
#include "wifi_rpc_common.h"

#include <android-base/logging.h>

#include "aidl_return_util.h"
#include "aidl_sync_util.h"
#include "wifi_status_util.h"
#include "wifi_feature_flags.h"

namespace {
// Starting Chip ID, will be assigned to primary chip
static constexpr int32_t kPrimaryChipId = 0;
}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
using aidl_return_util::validateAndCall;
using aidl_return_util::validateAndCallWithLock;
using aidl_sync_util::acquireGlobalLock;
using feature_flags::WifiFeatureFlags;

WifiRpc::WifiRpc()
    : WifiRpcRequest(),
      feature_flags_(std::make_shared<WifiFeatureFlags>()),
      run_state_(RunState::STOPPED) {
}

WifiRpc::~WifiRpc() {
    wifiRpcDeinit();
}

bool WifiRpc::isValid() {
    // This object is always valid.
    return true;
}

ndk::ScopedAStatus WifiRpc::registerEventCallback(
        const std::shared_ptr<IWifiEventCallback>& in_callback) {
    return validateAndCall(this, WifiStatusCode::ERROR_UNKNOWN,
                           &WifiRpc::registerEventCallbackInternal, in_callback);
}

ndk::ScopedAStatus WifiRpc::isStarted(bool* _aidl_return) {
    *_aidl_return = (run_state_ != RunState::STOPPED);
    return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus WifiRpc::start() {
    return validateAndCall(this, WifiStatusCode::ERROR_UNKNOWN, &WifiRpc::startInternal);
}

ndk::ScopedAStatus WifiRpc::stop() {
    return validateAndCallWithLock(this, WifiStatusCode::ERROR_UNKNOWN, &WifiRpc::stopInternal);
}

ndk::ScopedAStatus WifiRpc::getChipIds(std::vector<int32_t>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_UNKNOWN, &WifiRpc::getChipIdsInternal,
                           _aidl_return);
}

ndk::ScopedAStatus WifiRpc::getChip(int32_t in_chipId, std::shared_ptr<IWifiChip>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_UNKNOWN, &WifiRpc::getChipInternal,
                           _aidl_return, in_chipId);
}

std::set<std::shared_ptr<IWifiEventCallback>> WifiRpc::getEventCallbacks() {
    return event_cb_handler_.getCallbacks();
}

std::vector<std::shared_ptr<WifiChipRpc>> WifiRpc::getChips(){
	return chips_;
}

binder_status_t WifiRpc::dump(int fd, const char** args, uint32_t numArgs) {
    const auto lock = acquireGlobalLock();
    LOG(INFO) << "-----------Debug was called----------------";
    if (chips_.size() == 0) {
        LOG(INFO) << "No chips to display.";
        return STATUS_OK;
    }

    for (std::shared_ptr<WifiChipRpc> chip : chips_) {
        if (!chip.get()) continue;
        chip->dump(fd, args, numArgs);
    }
    return STATUS_OK;
}

ndk::ScopedAStatus WifiRpc::registerEventCallbackInternal(
        const std::shared_ptr<IWifiEventCallback>& event_callback) {
    if (!event_cb_handler_.addCallback(event_callback)) {
        return createWifiStatus(WifiStatusCode::ERROR_UNKNOWN);
    }
    return ndk::ScopedAStatus::ok();
}

std::pair<bool, ndk::ScopedAStatus> WifiRpc::isStartedInternal() {
    IsStartedCfmParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_IS_STARTED_REQ);
    if (rsp && WifiParseIsStartedCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result,  WifiParam2NdkStatus(param.status)};

    return {false, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};

}

ndk::ScopedAStatus WifiRpc::startInternal() {
    if (run_state_ == RunState::STARTED) {
        return ndk::ScopedAStatus::ok();
    } else if (run_state_ == RunState::STOPPING) {
        return createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE, "HAL is stopping");
    }
    ndk::ScopedAStatus wifi_status = wifiRpcStart();
    if (wifi_status.isOk()) {
        // Register the callback for subsystem restart
        const auto& on_subsystem_restart_callback = [this](const std::string& error) {
            ndk::ScopedAStatus wifi_status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, error);
            for (const auto& callback : event_cb_handler_.getCallbacks()) {
                LOG(INFO) << "Attempting to invoke onSubsystemRestart "
                             "callback";
                WifiStatusCode errorCode =
                        static_cast<WifiStatusCode>(wifi_status.getServiceSpecificError());
                if (!callback->onSubsystemRestart(errorCode).isOk()) {
                    LOG(ERROR) << "Failed to invoke onSubsystemRestart callback";
                } else {
                    LOG(INFO) << "Succeeded to invoke onSubsystemRestart "
                                 "callback";
                }
            }
        };

        // Create the chip instance once the HAL is started.
        int32_t chipId = kPrimaryChipId;

        // Here suppose legacy_hals_ has only one instance, so only chip will be created.
        chips_.push_back(
                WifiChipRpc::create(chipId, chipId == kPrimaryChipId, feature_flags_, on_subsystem_restart_callback, false));
        chipId++;
        run_state_ = RunState::STARTED;
        for (const auto& callback : event_cb_handler_.getCallbacks()) {
            if (!callback->onStart().isOk()) {
                LOG(ERROR) << "Failed to invoke onStart callback";
            };
        }
        LOG(INFO) << "Wifi HAL started";
    } else {
       for (const auto& callback : event_cb_handler_.getCallbacks()) {
            WifiStatusCode errorCode =
                    static_cast<WifiStatusCode>(wifi_status.getServiceSpecificError());
            if (!callback->onFailure(errorCode).isOk()) {
                LOG(ERROR) << "Failed to invoke onFailure callback";
            }
        }
        LOG(ERROR) << "Wifi HAL start failed";
        // Clear the event callback objects since the HAL start failed.
        event_cb_handler_.invalidate();
    }
    return wifi_status;
}

ndk::ScopedAStatus WifiRpc::wifiRpcStart()
{
    return SomeipMsgTransportWithReplyStatus(WIFI_START_REQ);
}

ndk::ScopedAStatus WifiRpc::stopInternal(
        /* NONNULL */ std::unique_lock<std::recursive_mutex>* lock) {
    if (run_state_ == RunState::STOPPED) {
        return ndk::ScopedAStatus::ok();
    } else if (run_state_ == RunState::STOPPING) {
        return createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE, "HAL is stopping");
    }
    // Clear the chip object and its child objects since the HAL is now
    // stopped.
    for (auto& chip : chips_) {
        if (chip.get()) {
            chip->invalidate();
            chip.reset();
        }
    }
    chips_.clear();
    ndk::ScopedAStatus wifi_status = wifiRpcStop(); //stopLegacyHalAndDeinitializeModeController(lock);
    if (wifi_status.isOk()) {
        for (const auto& callback : event_cb_handler_.getCallbacks()) {
            if (!callback->onStop().isOk()) {
                LOG(ERROR) << "Failed to invoke onStop callback";
            };
        }
        LOG(INFO) << "Wifi HAL stopped";
    } else {
        for (const auto& callback : event_cb_handler_.getCallbacks()) {
            WifiStatusCode errorCode =
                    static_cast<WifiStatusCode>(wifi_status.getServiceSpecificError());
            if (!callback->onFailure(errorCode).isOk()) {
                LOG(ERROR) << "Failed to invoke onFailure callback";
            }
        }
        LOG(ERROR) << "Wifi HAL stop failed";
    }
    // Clear the event callback objects since the HAL is now stopped.
    event_cb_handler_.invalidate();
    return wifi_status;
}

ndk::ScopedAStatus WifiRpc::wifiRpcStop()
{
    run_state_ = RunState::STOPPING;
    auto status = SomeipMsgTransportWithReplyStatus(WIFI_STOP_REQ);
    run_state_ = RunState::STOPPED;

    return status;
}

std::pair<std::vector<int32_t>, ndk::ScopedAStatus> WifiRpc::getChipIdsInternal() {
    std::vector<int32_t> chip_ids;

    for (auto& chip : chips_) {
        int32_t chip_id = getChipIdFromWifiChip(chip);
        if (chip_id != INT32_MAX) chip_ids.emplace_back(chip_id);
    }
    return {std::move(chip_ids), ndk::ScopedAStatus::ok()};
}

std::pair<std::shared_ptr<IWifiChip>, ndk::ScopedAStatus> WifiRpc::getChipInternal(int32_t chip_id) {
    for (auto& chip : chips_) {
        int32_t cand_id = getChipIdFromWifiChip(chip);
        if ((cand_id != INT32_MAX) && (cand_id == chip_id)) return {chip, ndk::ScopedAStatus::ok()};
    }

    return {nullptr, createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS)};
}

int32_t WifiRpc::getChipIdFromWifiChip(std::shared_ptr<WifiChipRpc>& chip) {
    int32_t chip_id = INT32_MAX;
    if (chip.get()) {
        ndk::ScopedAStatus status = chip->getId(&chip_id);
        if (!status.isOk()) {
            // Reset value if operation failed.
            chip_id = INT32_MAX;
        }
    }
    return chip_id;
}

}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
