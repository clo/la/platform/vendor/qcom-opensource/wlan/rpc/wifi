/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center, Inc. are provided under the following license:
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "wifi_chip_rpc.h"
#include "wifi_rpc_common.h"
#include "wifi_p2p_iface_rpc.h"
#include "someip_vlan_cfg.h"

#include <android-base/logging.h>
#include <android-base/unique_fd.h>
#include <cutils/properties.h>
#include <fcntl.h>
#include <net/if.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <wifi_system/interface_tool.h>

#include "aidl_return_util.h"
#include "aidl_struct_util.h"
#include "wifi_legacy_hal.h"
#include "wifi_status_util.h"

#define P2P_MGMT_DEVICE_PREFIX "p2p-dev-"

namespace {
using aidl::android::hardware::wifi::IfaceType;
using aidl::android::hardware::wifi::IWifiChip;
using ::android::wifi_system::InterfaceTool;
using CoexRestriction = aidl::android::hardware::wifi::IWifiChip::CoexRestriction;
using ChannelCategoryMask = aidl::android::hardware::wifi::IWifiChip::ChannelCategoryMask;
using android::base::unique_fd;

constexpr char kCpioMagic[] = "070701";
constexpr size_t kMaxBufferSizeBytes = 1024 * 1024 * 3;
constexpr uint32_t kMaxRingBufferFileAgeSeconds = 60 * 60 * 10;
constexpr uint32_t kMaxRingBufferFileNum = 20;
constexpr char kTombstoneFolderPath[] = "/data/vendor/tombstones/wifi/";
constexpr char kActiveWlanIfaceNameProperty[] = "wifi.active.interface";
constexpr char kNoActiveWlanIfaceNamePropertyValue[] = "";
constexpr unsigned kMaxWlanIfaces = 5;
constexpr char kApBridgeIfacePrefix[] = "ap_br_";

template <typename Iface>
void invalidateAndClear(std::vector<std::shared_ptr<Iface>>& ifaces, std::shared_ptr<Iface> iface) {
    iface->invalidate();
    ifaces.erase(std::remove(ifaces.begin(), ifaces.end(), iface), ifaces.end());
}

template <typename Iface>
void invalidateAndClearAll(std::vector<std::shared_ptr<Iface>>& ifaces) {
    for (const auto& iface : ifaces) {
        iface->invalidate();
    }
    ifaces.clear();
}

template <typename Iface>
std::vector<std::string> getNames(std::vector<std::shared_ptr<Iface>>& ifaces) {
    std::vector<std::string> names;
    for (const auto& iface : ifaces) {
        names.emplace_back(iface->getName());
    }
    return names;
}

template <typename Iface>
std::shared_ptr<Iface> findUsingName(std::vector<std::shared_ptr<Iface>>& ifaces,
                                     const std::string& name) {
    std::vector<std::string> names;
    for (const auto& iface : ifaces) {
        if (name == iface->getName()) {
            return iface;
        }
    }
    return nullptr;
}

std::string getWlanIfaceName(unsigned idx) {
    if (idx >= kMaxWlanIfaces) {
        CHECK(false) << "Requested interface beyond wlan" << kMaxWlanIfaces;
        return {};
    }

    std::array<char, PROPERTY_VALUE_MAX> buffer;
    if (idx == 0 || idx == 1) {
        const char* altPropName = (idx == 0) ? "wifi.interface" : "wifi.concurrent.interface";
        auto res = property_get(altPropName, buffer.data(), nullptr);
        if (res > 0) return buffer.data();
    }
    std::string propName = "wifi.interface." + std::to_string(idx);
    auto res = property_get(propName.c_str(), buffer.data(), nullptr);
    if (res > 0) return buffer.data();

    return "wlan" + std::to_string(idx);
}

// Returns the dedicated iface name if defined.
// Returns two ifaces in bridged mode.
std::vector<std::string> getPredefinedApIfaceNames(bool is_bridged) {
    std::vector<std::string> ifnames;
    std::array<char, PROPERTY_VALUE_MAX> buffer;
    buffer.fill(0);
    if (property_get("ro.vendor.wifi.sap.interface", buffer.data(), nullptr) == 0) {
        return ifnames;
    }
    ifnames.push_back(buffer.data());
    if (is_bridged) {
        buffer.fill(0);
        if (property_get("ro.vendor.wifi.sap.concurrent.iface", buffer.data(), nullptr) == 0) {
            return ifnames;
        }
        ifnames.push_back(buffer.data());
    }
    return ifnames;
}

std::string getPredefinedP2pIfaceName() {
    std::array<char, PROPERTY_VALUE_MAX> primaryIfaceName;
    char p2pParentIfname[100];
    std::string p2pDevIfName = "";
    std::array<char, PROPERTY_VALUE_MAX> buffer;
    property_get("wifi.direct.interface", buffer.data(), "p2p0");
    if (strncmp(buffer.data(), P2P_MGMT_DEVICE_PREFIX, strlen(P2P_MGMT_DEVICE_PREFIX)) == 0) {
        /* Get the p2p parent interface name from p2p device interface name set
         * in property */
        strlcpy(p2pParentIfname, buffer.data() + strlen(P2P_MGMT_DEVICE_PREFIX),
                strlen(buffer.data()) - strlen(P2P_MGMT_DEVICE_PREFIX));
        if (property_get(kActiveWlanIfaceNameProperty, primaryIfaceName.data(), nullptr) == 0) {
            return buffer.data();
        }
        /* Check if the parent interface derived from p2p device interface name
         * is active */
        if (strncmp(p2pParentIfname, primaryIfaceName.data(),
                    strlen(buffer.data()) - strlen(P2P_MGMT_DEVICE_PREFIX)) != 0) {
            /*
             * Update the predefined p2p device interface parent interface name
             * with current active wlan interface
             */
            p2pDevIfName += P2P_MGMT_DEVICE_PREFIX;
            p2pDevIfName += primaryIfaceName.data();
            LOG(INFO) << "update the p2p device interface name to " << p2pDevIfName.c_str();
            return p2pDevIfName;
        }
    }
    return buffer.data();
}

// Returns the dedicated iface name if one is defined.
std::string getPredefinedNanIfaceName() {
    std::array<char, PROPERTY_VALUE_MAX> buffer;
    if (property_get("wifi.aware.interface", buffer.data(), nullptr) == 0) {
        return {};
    }
    return buffer.data();
}

void setActiveWlanIfaceNameProperty(const std::string& ifname) {
    auto res = property_set(kActiveWlanIfaceNameProperty, ifname.data());
    if (res != 0) {
        PLOG(ERROR) << "Failed to set active wlan iface name property";
    }
}

// Delete files that meet either condition:
// 1. Older than a predefined time in the wifi tombstone dir.
// 2. Files in excess to a predefined amount, starting from the oldest ones
bool removeOldFilesInternal() {
    time_t now = time(0);
    const time_t delete_files_before = now - kMaxRingBufferFileAgeSeconds;
    std::unique_ptr<DIR, decltype(&closedir)> dir_dump(opendir(kTombstoneFolderPath), closedir);
    if (!dir_dump) {
        PLOG(ERROR) << "Failed to open directory";
        return false;
    }
    struct dirent* dp;
    bool success = true;
    std::list<std::pair<const time_t, std::string>> valid_files;
    while ((dp = readdir(dir_dump.get()))) {
        if (dp->d_type != DT_REG) {
            continue;
        }
        std::string cur_file_name(dp->d_name);
        struct stat cur_file_stat;
        std::string cur_file_path = kTombstoneFolderPath + cur_file_name;
        if (stat(cur_file_path.c_str(), &cur_file_stat) == -1) {
            PLOG(ERROR) << "Failed to get file stat for " << cur_file_path;
            success = false;
            continue;
        }
        const time_t cur_file_time = cur_file_stat.st_mtime;
        valid_files.push_back(std::pair<const time_t, std::string>(cur_file_time, cur_file_path));
    }
    valid_files.sort();  // sort the list of files by last modified time from
                         // small to big.
    uint32_t cur_file_count = valid_files.size();
    for (auto cur_file : valid_files) {
        if (cur_file_count > kMaxRingBufferFileNum || cur_file.first < delete_files_before) {
            if (unlink(cur_file.second.c_str()) != 0) {
                PLOG(ERROR) << "Error deleting file";
                success = false;
            }
            cur_file_count--;
        } else {
            break;
        }
    }
    return success;
}

// Helper function for |cpioArchiveFilesInDir|
bool cpioWriteHeader(int out_fd, struct stat& st, const char* file_name, size_t file_name_len) {
    const int buf_size = 32 * 1024;
    std::array<char, buf_size> read_buf;
    ssize_t llen = snprintf(
            read_buf.data(), buf_size, "%s%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X",
            kCpioMagic, static_cast<int>(st.st_ino), st.st_mode, st.st_uid, st.st_gid,
            static_cast<int>(st.st_nlink), static_cast<int>(st.st_mtime),
            static_cast<int>(st.st_size), major(st.st_dev), minor(st.st_dev), major(st.st_rdev),
            minor(st.st_rdev), static_cast<uint32_t>(file_name_len), 0);
    if (write(out_fd, read_buf.data(), llen < buf_size ? llen : buf_size - 1) == -1) {
        PLOG(ERROR) << "Error writing cpio header to file " << file_name;
        return false;
    }
    if (write(out_fd, file_name, file_name_len) == -1) {
        PLOG(ERROR) << "Error writing filename to file " << file_name;
        return false;
    }

    // NUL Pad header up to 4 multiple bytes.
    llen = (llen + file_name_len) % 4;
    if (llen != 0) {
        const uint32_t zero = 0;
        if (write(out_fd, &zero, 4 - llen) == -1) {
            PLOG(ERROR) << "Error padding 0s to file " << file_name;
            return false;
        }
    }
    return true;
}

// Helper function for |cpioArchiveFilesInDir|
size_t cpioWriteFileContent(int fd_read, int out_fd, struct stat& st) {
    // writing content of file
    std::array<char, 32 * 1024> read_buf;
    ssize_t llen = st.st_size;
    size_t n_error = 0;
    while (llen > 0) {
        ssize_t bytes_read = read(fd_read, read_buf.data(), read_buf.size());
        if (bytes_read == -1) {
            PLOG(ERROR) << "Error reading file";
            return ++n_error;
        }
        llen -= bytes_read;
        if (write(out_fd, read_buf.data(), bytes_read) == -1) {
            PLOG(ERROR) << "Error writing data to file";
            return ++n_error;
        }
        if (bytes_read == 0) {  // this should never happen, but just in case
                                // to unstuck from while loop
            PLOG(ERROR) << "Unexpected read result";
            n_error++;
            break;
        }
    }
    llen = st.st_size % 4;
    if (llen != 0) {
        const uint32_t zero = 0;
        if (write(out_fd, &zero, 4 - llen) == -1) {
            PLOG(ERROR) << "Error padding 0s to file";
            return ++n_error;
        }
    }
    return n_error;
}

// Helper function for |cpioArchiveFilesInDir|
bool cpioWriteFileTrailer(int out_fd) {
    const int buf_size = 4096;
    std::array<char, buf_size> read_buf;
    read_buf.fill(0);
    ssize_t llen = snprintf(read_buf.data(), 4096, "070701%040X%056X%08XTRAILER!!!", 1, 0x0b, 0);
    if (write(out_fd, read_buf.data(), (llen < buf_size ? llen : buf_size - 1) + 4) == -1) {
        PLOG(ERROR) << "Error writing trailing bytes";
        return false;
    }
    return true;
}

// Archives all files in |input_dir| and writes result into |out_fd|
// Logic obtained from //external/toybox/toys/posix/cpio.c "Output cpio archive"
// portion
size_t cpioArchiveFilesInDir(int out_fd, const char* input_dir) {
    struct dirent* dp;
    size_t n_error = 0;
    std::unique_ptr<DIR, decltype(&closedir)> dir_dump(opendir(input_dir), closedir);
    if (!dir_dump) {
        PLOG(ERROR) << "Failed to open directory";
        return ++n_error;
    }
    while ((dp = readdir(dir_dump.get()))) {
        if (dp->d_type != DT_REG) {
            continue;
        }
        std::string cur_file_name(dp->d_name);
        struct stat st;
        const std::string cur_file_path = kTombstoneFolderPath + cur_file_name;
        if (stat(cur_file_path.c_str(), &st) == -1) {
            PLOG(ERROR) << "Failed to get file stat for " << cur_file_path;
            n_error++;
            continue;
        }
        const int fd_read = open(cur_file_path.c_str(), O_RDONLY);
        if (fd_read == -1) {
            PLOG(ERROR) << "Failed to open file " << cur_file_path;
            n_error++;
            continue;
        }
        std::string file_name_with_last_modified_time =
                cur_file_name + "-" + std::to_string(st.st_mtime);
        // string.size() does not include the null terminator. The cpio FreeBSD
        // file header expects the null character to be included in the length.
        const size_t file_name_len = file_name_with_last_modified_time.size() + 1;
        unique_fd file_auto_closer(fd_read);
        if (!cpioWriteHeader(out_fd, st, file_name_with_last_modified_time.c_str(),
                             file_name_len)) {
            return ++n_error;
        }
        size_t write_error = cpioWriteFileContent(fd_read, out_fd, st);
        if (write_error) {
            return n_error + write_error;
        }
    }
    if (!cpioWriteFileTrailer(out_fd)) {
        return ++n_error;
    }
    return n_error;
}

// Helper function to create a non-const char*.
std::vector<char> makeCharVec(const std::string& str) {
    std::vector<char> vec(str.size() + 1);
    vec.assign(str.begin(), str.end());
    vec.push_back('\0');
    return vec;
}

}  // namespace

namespace aidl {
namespace android {
namespace hardware {
namespace wifi {
using aidl_return_util::validateAndCall;
using aidl_return_util::validateAndCallWithLock;

WifiChipRpc::WifiChipRpc(int32_t chip_id, bool is_primary,
                   const std::weak_ptr<feature_flags::WifiFeatureFlags> feature_flags,
                   const std::function<void(const std::string&)>& handler,
                   bool using_dynamic_iface_combination)
    : WifiRpcRequest(chip_id),
      chip_id_(chip_id),
      is_valid_(true),
      current_mode_id_(feature_flags::chip_mode_ids::kInvalid),
      modes_(feature_flags.lock()->getChipModes(is_primary)),
      debug_ring_buffer_cb_registered_(false),
      using_dynamic_iface_combination_(using_dynamic_iface_combination),
      subsystemCallbackHandler_(handler) {
      setActiveWlanIfaceNameProperty(kNoActiveWlanIfaceNamePropertyValue);
}

std::shared_ptr<WifiChipRpc> WifiChipRpc::create(
        int32_t chip_id, bool is_primary,
        const std::weak_ptr<feature_flags::WifiFeatureFlags> feature_flags,
        const std::function<void(const std::string&)>& handler,
        bool using_dynamic_iface_combination) {
    std::shared_ptr<WifiChipRpc> ptr = ndk::SharedRefBase::make<WifiChipRpc>(
            chip_id, is_primary, feature_flags, handler,
            using_dynamic_iface_combination);
    std::weak_ptr<WifiChipRpc> weak_ptr_this(ptr);
    ptr->setWeakPtr(weak_ptr_this);
    return ptr;
}

void WifiChipRpc::invalidate() {
//    if (!writeRingbufferFilesInternal()) {
//        LOG(ERROR) << "Error writing files to flash";
//    }
    invalidateAndRemoveAllIfaces();
    setActiveWlanIfaceNameProperty(kNoActiveWlanIfaceNamePropertyValue);
    event_cb_handler_.invalidate();
    is_valid_ = false;
}

void WifiChipRpc::setWeakPtr(std::weak_ptr<WifiChipRpc> ptr) {
    weak_ptr_this_ = ptr;
}

bool WifiChipRpc::isValid() {
    return is_valid_;
}

std::set<std::shared_ptr<IWifiChipEventCallback>> WifiChipRpc::getEventCallbacks() {
    return event_cb_handler_.getCallbacks();
}

std::vector<std::shared_ptr<WifiStaIfaceRpc>> WifiChipRpc::getStaIfacesPointer() {
	return  sta_ifaces_;
}

ndk::ScopedAStatus WifiChipRpc::getId(int32_t* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID, &WifiChipRpc::getIdInternal,
                           _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::registerEventCallback(
        const std::shared_ptr<IWifiChipEventCallback>& event_callback) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::registerEventCallbackInternal, event_callback);
}

ndk::ScopedAStatus WifiChipRpc::getFeatureSet(int32_t* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getFeatureSetInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getAvailableModes(std::vector<IWifiChip::ChipMode>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getAvailableModesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::configureChip(int32_t in_modeId) {
    return validateAndCallWithLock(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                                   &WifiChipRpc::configureChipInternal, in_modeId);
}

ndk::ScopedAStatus WifiChipRpc::getMode(int32_t* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getModeInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::requestChipDebugInfo(IWifiChip::ChipDebugInfo* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::requestChipDebugInfoInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::requestDriverDebugDump(std::vector<uint8_t>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::requestDriverDebugDumpInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::requestFirmwareDebugDump(std::vector<uint8_t>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::requestFirmwareDebugDumpInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::createApIface(std::shared_ptr<IWifiApIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createApIfaceInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::createBridgedApIface(std::shared_ptr<IWifiApIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createBridgedApIfaceInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getApIfaceNames(std::vector<std::string>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getApIfaceNamesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getApIface(const std::string& in_ifname,
                                        std::shared_ptr<IWifiApIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getApIfaceInternal, _aidl_return, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::removeApIface(const std::string& in_ifname) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::removeApIfaceInternal, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::removeIfaceInstanceFromBridgedApIface(
        const std::string& in_brIfaceName, const std::string& in_ifaceInstanceName) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::removeIfaceInstanceFromBridgedApIfaceInternal, in_brIfaceName,
                           in_ifaceInstanceName);
}

ndk::ScopedAStatus WifiChipRpc::createNanIface(std::shared_ptr<IWifiNanIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createNanIfaceInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getNanIfaceNames(std::vector<std::string>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getNanIfaceNamesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getNanIface(const std::string& in_ifname,
                                         std::shared_ptr<IWifiNanIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getNanIfaceInternal, _aidl_return, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::removeNanIface(const std::string& in_ifname) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::removeNanIfaceInternal, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::createP2pIface(std::shared_ptr<IWifiP2pIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createP2pIfaceInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getP2pIfaceNames(std::vector<std::string>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getP2pIfaceNamesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getP2pIface(const std::string& in_ifname,
                                         std::shared_ptr<IWifiP2pIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getP2pIfaceInternal, _aidl_return, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::removeP2pIface(const std::string& in_ifname) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::removeP2pIfaceInternal, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::createStaIface(std::shared_ptr<IWifiStaIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createStaIfaceInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getStaIfaceNames(std::vector<std::string>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getStaIfaceNamesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getStaIface(const std::string& in_ifname,
                                         std::shared_ptr<IWifiStaIface>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getStaIfaceInternal, _aidl_return, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::removeStaIface(const std::string& in_ifname) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::removeStaIfaceInternal, in_ifname);
}

ndk::ScopedAStatus WifiChipRpc::createRttController(
        const std::shared_ptr<IWifiStaIface>& in_boundIface,
        std::shared_ptr<IWifiRttController>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::createRttControllerInternal, _aidl_return, in_boundIface);
}

ndk::ScopedAStatus WifiChipRpc::getDebugRingBuffersStatus(
        std::vector<WifiDebugRingBufferStatus>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getDebugRingBuffersStatusInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::startLoggingToDebugRingBuffer(
        const std::string& in_ringName, WifiDebugRingBufferVerboseLevel in_verboseLevel,
        int32_t in_maxIntervalInSec, int32_t in_minDataSizeInBytes) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::startLoggingToDebugRingBufferInternal, in_ringName,
                           in_verboseLevel, in_maxIntervalInSec, in_minDataSizeInBytes);
}

ndk::ScopedAStatus WifiChipRpc::forceDumpToDebugRingBuffer(const std::string& in_ringName) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::forceDumpToDebugRingBufferInternal, in_ringName);
}

ndk::ScopedAStatus WifiChipRpc::flushRingBufferToFile() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::flushRingBufferToFileInternal);
}

ndk::ScopedAStatus WifiChipRpc::stopLoggingToDebugRingBuffer() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::stopLoggingToDebugRingBufferInternal);
}

ndk::ScopedAStatus WifiChipRpc::getDebugHostWakeReasonStats(
        WifiDebugHostWakeReasonStats* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getDebugHostWakeReasonStatsInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::enableDebugErrorAlerts(bool in_enable) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::enableDebugErrorAlertsInternal, in_enable);
}

ndk::ScopedAStatus WifiChipRpc::selectTxPowerScenario(IWifiChip::TxPowerScenario in_scenario) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::selectTxPowerScenarioInternal, in_scenario);
}

ndk::ScopedAStatus WifiChipRpc::resetTxPowerScenario() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::resetTxPowerScenarioInternal);
}

ndk::ScopedAStatus WifiChipRpc::setLatencyMode(IWifiChip::LatencyMode in_mode) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setLatencyModeInternal, in_mode);
}

binder_status_t WifiChipRpc::dump(int fd, const char**, uint32_t) {
#if 0
    {
        std::unique_lock<std::mutex> lk(lock_t);
        for (const auto& item : ringbuffer_map_) {
            forceDumpToDebugRingBufferInternal(item.first);
        }
        // unique_lock unlocked here
    }
    usleep(100 * 1000);  // sleep for 100 milliseconds to wait for
                         // ringbuffer updates.
    if (!writeRingbufferFilesInternal()) {
        LOG(ERROR) << "Error writing files to flash";
    }
    uint32_t n_error = cpioArchiveFilesInDir(fd, kTombstoneFolderPath);
    if (n_error != 0) {
        LOG(ERROR) << n_error << " errors occurred in cpio function";
    }
    fsync(fd);
#endif
    return STATUS_OK;
}

ndk::ScopedAStatus WifiChipRpc::setMultiStaPrimaryConnection(const std::string& in_ifName) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setMultiStaPrimaryConnectionInternal, in_ifName);
}

ndk::ScopedAStatus WifiChipRpc::setMultiStaUseCase(IWifiChip::MultiStaUseCase in_useCase) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setMultiStaUseCaseInternal, in_useCase);
}

ndk::ScopedAStatus WifiChipRpc::setCoexUnsafeChannels(
        const std::vector<IWifiChip::CoexUnsafeChannel>& in_unsafeChannels,
        int32_t in_restrictions) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setCoexUnsafeChannelsInternal, in_unsafeChannels,
                           in_restrictions);
}

ndk::ScopedAStatus WifiChipRpc::setCountryCode(const std::array<uint8_t, 2>& in_code) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_IFACE_INVALID,
                           &WifiChipRpc::setCountryCodeInternal, in_code);
}

ndk::ScopedAStatus WifiChipRpc::getUsableChannels(WifiBand in_band, int32_t in_ifaceModeMask,
                                               int32_t in_filterMask,
                                               std::vector<WifiUsableChannel>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getUsableChannelsInternal, _aidl_return, in_band,
                           in_ifaceModeMask, in_filterMask);
}

ndk::ScopedAStatus WifiChipRpc::setAfcChannelAllowance(
        const AfcChannelAllowance& afcChannelAllowance) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setAfcChannelAllowanceInternal, afcChannelAllowance);
}

ndk::ScopedAStatus WifiChipRpc::triggerSubsystemRestart() {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::triggerSubsystemRestartInternal);
}

ndk::ScopedAStatus WifiChipRpc::getSupportedRadioCombinations(
        std::vector<WifiRadioCombination>* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getSupportedRadioCombinationsInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::getWifiChipCapabilities(WifiChipCapabilities* _aidl_return) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::getWifiChipCapabilitiesInternal, _aidl_return);
}

ndk::ScopedAStatus WifiChipRpc::enableStaChannelForPeerNetwork(int32_t in_channelCategoryEnableFlag) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::enableStaChannelForPeerNetworkInternal,
                           in_channelCategoryEnableFlag);
}

ndk::ScopedAStatus WifiChipRpc::setMloMode(const ChipMloMode in_mode) {
    return validateAndCall(this, WifiStatusCode::ERROR_WIFI_CHIP_INVALID,
                           &WifiChipRpc::setMloModeInternal, in_mode);
}

void WifiChipRpc::invalidateAndRemoveAllIfaces() {
    invalidateAndClearBridgedApAll();
    invalidateAndClearAll(ap_ifaces_);
    invalidateAndClearAll(nan_ifaces_);
    invalidateAndClearAll(p2p_ifaces_);
    invalidateAndClearAll(sta_ifaces_);
    ap_ifaces_map.clear();
    sta_ifaces_map.clear();
    // Since all the ifaces are invalid now, all RTT controller objects
    // using those ifaces also need to be invalidated.
    for (const auto& rtt : rtt_controllers_) {
        rtt->invalidate();
    }
    rtt_controllers_.clear();
}

void WifiChipRpc::invalidateAndRemoveDependencies(const std::string& removed_iface_name) {
    for (auto it = nan_ifaces_.begin(); it != nan_ifaces_.end();) {
        auto nan_iface = *it;
        if (nan_iface->getName() == removed_iface_name) {
            nan_iface->invalidate();
            for (const auto& callback : event_cb_handler_.getCallbacks()) {
                if (!callback->onIfaceRemoved(IfaceType::NAN_IFACE, removed_iface_name).isOk()) {
                    LOG(ERROR) << "Failed to invoke onIfaceRemoved callback";
                }
            }
            it = nan_ifaces_.erase(it);
        } else {
            ++it;
        }
    }

    for (auto it = rtt_controllers_.begin(); it != rtt_controllers_.end();) {
        auto rtt = *it;
        if (rtt->getIfaceName() == removed_iface_name) {
            rtt->invalidate();
            it = rtt_controllers_.erase(it);
        } else {
            ++it;
        }
    }
}

std::pair<int32_t, ndk::ScopedAStatus> WifiChipRpc::getIdInternal() {
    return {chip_id_, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::registerEventCallbackInternal(
        const std::shared_ptr<IWifiChipEventCallback>& event_callback) {
    if (!event_cb_handler_.addCallback(event_callback)) {
        return createWifiStatus(WifiStatusCode::ERROR_UNKNOWN);
    }
    return ndk::ScopedAStatus::ok();
}

std::pair<int32_t, ndk::ScopedAStatus> WifiChipRpc::getFeatureSetInternal() {
    GetFeatureSetCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_FEATURE_SET_REQ);
    if (rsp && WifiChipParseGetFeatureSetCfm(rsp->getData(), rsp->getLength(), param))
	    return {param.result, WifiParam2NdkStatus(param.status)};

    return {0, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

std::pair<std::vector<IWifiChip::ChipMode>, ndk::ScopedAStatus>
WifiChipRpc::getAvailableModesInternal() {
    return {modes_, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::configureChipInternal(
        /* NONNULL */ std::unique_lock<std::recursive_mutex>* lock, int32_t mode_id) {
    if (!isValidModeId(mode_id)) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }
    if (mode_id == current_mode_id_) {
        LOG(DEBUG) << "Already in the specified mode " << mode_id;
        return ndk::ScopedAStatus::ok();
    }

    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiChipSerializeConfigureChipReq(mode_id, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_CONFIGURE_CHIP_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");
    if (!status.isOk()) {
        WifiStatusCode errorCode = static_cast<WifiStatusCode>(status.getServiceSpecificError());
        for (const auto& callback : event_cb_handler_.getCallbacks()) {
            if (!callback->onChipReconfigureFailure(errorCode).isOk()) {
                LOG(ERROR) << "Failed to invoke onChipReconfigureFailure callback";
            }
        }
        return status;
    }

    /* Chip mode re-configured, update local modes from rpc */
    payload.clear();
    GetAvailableModesCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_AVAILABLE_MODES_REQ);
    if (rsp && WifiChipParseGetAvailableModesCfm(rsp->getData(), rsp->getLength(), param)) {
        modes_.clear();
        for (auto& mode : param.result)
            modes_.push_back(mode);
    } else {
        LOG(ERROR) << "Failed to get available modes from rpc";
    }

    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onChipReconfigured(mode_id).isOk()) {
            LOG(ERROR) << "Failed to invoke onChipReconfigured callback";
        }
    }
    current_mode_id_ = mode_id;
    LOG(INFO) << "Configured chip in mode " << mode_id;

    return status;
}

std::pair<int32_t, ndk::ScopedAStatus> WifiChipRpc::getModeInternal() {
    if (!isValidModeId(current_mode_id_)) {
        return {current_mode_id_, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }
    return {current_mode_id_, ndk::ScopedAStatus::ok()};
}

std::pair<IWifiChip::ChipDebugInfo, ndk::ScopedAStatus> WifiChipRpc::requestChipDebugInfoInternal() {
    RequestChipDebugInfoCfmChipParam param;
    IWifiChip::ChipDebugInfo result;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_REQUEST_CHIP_DEBUG_INFO_REQ);
    if (rsp && WifiChipParseRequestChipDebugInfoCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};
    return {std::move(result), createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};

}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus> WifiChipRpc::requestDriverDebugDumpInternal() {
    RequestDriverDebugDumpCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_REQUEST_DRIVER_DEBUG_DUMP_REQ);
    if (rsp && WifiChipParseRequestDriverDebugDumpCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<uint8_t>(), createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)}; 
}

std::pair<std::vector<uint8_t>, ndk::ScopedAStatus> WifiChipRpc::requestFirmwareDebugDumpInternal() {
    RequestFirmwareDebugDumpCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_REQUEST_FIRMWARE_DEBUG_DUMP_REQ);
    if (rsp && WifiChipParseRequestFirmwareDebugDumpCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<uint8_t>(), createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)}; 
}

std::shared_ptr<WifiApIfaceRpc> WifiChipRpc::newWifiApIface(std::string& ifname, int32_t apInstanceId) {
    std::vector<std::string> ap_instances;
    for (auto const& it : br_ifaces_ap_instances_) {
        if (it.first == ifname) {
            ap_instances = it.second;
        }
    }

#ifdef WIFI_RPC_CEM
    if (!createVlanInf(ifname, kCemBrApVlanId))
#else
    if (!createVlanInf(ifname, kChmBrApVlanId))
#endif
        return nullptr;

    std::shared_ptr<WifiApIfaceRpc> iface =
        ndk::SharedRefBase::make<WifiApIfaceRpc>(ifname, ap_instances, chip_id_, apInstanceId);
    ap_ifaces_.push_back(iface);
    ap_ifaces_map[iface] = apInstanceId;
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceAdded(IfaceType::AP, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceAdded callback";
        }
    }
    //setActiveWlanIfaceNameProperty(getFirstActiveWlanIfaceName());
    return iface;
}

std::pair<std::shared_ptr<IWifiApIface>, ndk::ScopedAStatus> WifiChipRpc::createApIfaceInternal() {
    if (!canCurrentModeSupportConcurrencyTypeWithCurrentTypes(IfaceConcurrencyType::AP)) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }

    int32_t apInstanceId;
    ndk::ScopedAStatus status;
    CreateApIfaceCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_CREATE_AP_IFACE_REQ);
    if (rsp && WifiChipParseCreateApIfaceCfm(rsp->getData(), rsp->getLength(), param)){
        apInstanceId = param.result;
        status = WifiParam2NdkStatus(param.status);
    }

    if (!status.isOk()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN,
            "Proto message CreateApIfaceCfm parse failure")};
    }

    if (apInstanceId & 0xffff0000) {
        LOG(ERROR) << "RPC created ap iface with invalid instance id 0x" << std::hex << apInstanceId;
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    vector<string> ap_instances;
    GetBridgedInstancesCfmApIfaceParam bridgedApParam;
    WifiRpcRequest request(chip_id_, apInstanceId);
    rsp = request.SomeipMsgTransportWithReplyMsg(WIFI_AP_IFACE_GET_BRIDGED_INSTANCES_REQ);
    if (rsp && WifiApIfaceParseGetBridgedInstancesCfm(rsp->getData(), rsp->getLength(), bridgedApParam)) {
        ap_instances = bridgedApParam.result;
        status = WifiParam2NdkStatus(bridgedApParam.status);
    }
    if (!status.isOk()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN,
            "Proto message GetBridgedInstancesCfm parse failure")};
    }
    //only one instance: wlan2. Here remote side return ap_br_wlan2
    //which add wlan2 and vlanx into bridge.
    if (ap_instances.size() < 1) {
        LOG(ERROR) << "Fail to allocate instance";
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }

    for (auto const& instance : ap_instances) {
        if (!createVlanInf(instance))
            return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    std::string br_ifname = kApBridgeIfacePrefix + ap_instances[0];
    br_ifaces_ap_instances_[br_ifname] = ap_instances;
    std::shared_ptr<WifiApIfaceRpc> iface = newWifiApIface(br_ifname, apInstanceId);
    return {iface, ndk::ScopedAStatus::ok()};

}

std::pair<std::shared_ptr<IWifiApIface>, ndk::ScopedAStatus>
WifiChipRpc::createBridgedApIfaceInternal() {
    if (!canCurrentModeSupportConcurrencyTypeWithCurrentTypes(IfaceConcurrencyType::AP_BRIDGED)) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }

    int32_t apInstanceId;
    ndk::ScopedAStatus status;
    CreateBridgedApIfaceCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_CREATE_BRIDGED_AP_IFACE_REQ);
    if (rsp && WifiChipParseCreateBridgedApIfaceCfm(rsp->getData(), rsp->getLength(), param)) {
        apInstanceId = param.result;
        status = WifiParam2NdkStatus(param.status);
    }
    if (!status.isOk()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN,
            "Proto message CreateBridgedApIfaceCfm parse failure")};
    }

    if (apInstanceId & 0xffff0000) {
        LOG(ERROR) << "RPC created ap iface with invalid instance id 0x" << std::hex << apInstanceId;
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    vector<string> ap_instances;
    GetBridgedInstancesCfmApIfaceParam bridgedApParam;
    WifiRpcRequest request(chip_id_, apInstanceId);
    rsp = request.SomeipMsgTransportWithReplyMsg(WIFI_AP_IFACE_GET_BRIDGED_INSTANCES_REQ);
    if (rsp && WifiApIfaceParseGetBridgedInstancesCfm(rsp->getData(), rsp->getLength(), bridgedApParam)) {
        ap_instances = bridgedApParam.result;
        status = WifiParam2NdkStatus(bridgedApParam.status);
    }
    if (!status.isOk()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN,
            "Proto message GetBridgedInstancesCfm parse failure")};
    }
    if (ap_instances.size() < 2) {
        LOG(ERROR) << "Fail to allocate two instances";
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }

    for (auto const& instance : ap_instances) {
        if (!createVlanInf(instance))
            return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    std::string br_ifname = kApBridgeIfacePrefix + ap_instances[0];
    br_ifaces_ap_instances_[br_ifname] = ap_instances;
    std::shared_ptr<WifiApIfaceRpc> iface = newWifiApIface(br_ifname, apInstanceId);
    return {iface, ndk::ScopedAStatus::ok()};
}

std::pair<std::vector<std::string>, ndk::ScopedAStatus> WifiChipRpc::getApIfaceNamesInternal() {
    if (ap_ifaces_.empty()) {
        return {std::vector<std::string>(), ndk::ScopedAStatus::ok()};
    }
    return {getNames(ap_ifaces_), ndk::ScopedAStatus::ok()};
}

std::pair<std::shared_ptr<IWifiApIface>, ndk::ScopedAStatus> WifiChipRpc::getApIfaceInternal(
        const std::string& ifname) {
    const auto iface = findUsingName(ap_ifaces_, ifname);
    if (!iface.get()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS)};
    }
    return {iface, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::removeApIfaceInternal(const std::string& ifname) {
    const auto iface = findUsingName(ap_ifaces_, ifname);
    if (!iface.get()) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }

    vector<uint8_t> payload;
    if (WifiChipSerializeRemoveApIfaceReq(ifname, payload)) {
        //Reset timeout value for bridged AP, as rpc need more time to clear bridged AP.
        uint32_t timeout = br_ifaces_ap_instances_.count(ifname) ? 2500 : 500;
        auto status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_REMOVE_AP_IFACE_REQ, payload, timeout);
        if (!status.isOk())
            LOG(ERROR) << "RPC failed to remove AP interface " << ifname;
    }

    // Invalidate & remove any dependent objects first.
    // Note: This is probably not required because we never create
    // nan/rtt objects over AP iface. But, there is no harm to do it
    // here and not make that assumption all over the place.
    invalidateAndRemoveDependencies(ifname);
    deleteApIface(ifname);
    invalidateAndClear(ap_ifaces_, iface);
    ap_ifaces_map.erase(iface);
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceRemoved(IfaceType::AP, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceRemoved callback";
        }
    }

    //setActiveWlanIfaceNameProperty(getFirstActiveWlanIfaceName());
    return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus WifiChipRpc::removeIfaceInstanceFromBridgedApIfaceInternal(
        const std::string& ifname, const std::string& ifInstanceName) {
    const auto iface = findUsingName(ap_ifaces_, ifname);
    if (!iface.get() || ifInstanceName.empty()) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }
    // Requires to remove one of the instance in bridge mode
    vector<uint8_t> payload;
    if (WifiChipSerializeRemoveIfaceInstanceFromBridgedApIfaceReq(ifname,
        ifInstanceName, payload)) {
        auto status = SomeipMsgTransportWithReplyStatus(
            WIFI_CHIP_REMOVE_IFACE_INSTANCE_FROM_BRIDGED_AP_IFACE_REQ, payload);
        if (!status.isOk()) {
            LOG(ERROR) << "RPC failed to remove interface: " << ifInstanceName
                       << " from " << ifname;
        }
    }

    for (auto const& it : br_ifaces_ap_instances_) {
        if (it.first == ifname) {
            std::vector<std::string> ap_instances = it.second;
            for (auto const& instance : ap_instances) {
                if (instance == ifInstanceName) {
                    deleteVlanInf(ifInstanceName);
                    ap_instances.erase(
                        std::remove(ap_instances.begin(), ap_instances.end(), ifInstanceName),
                        ap_instances.end());
                    br_ifaces_ap_instances_[ifname] = ap_instances;
                    break;
                }
            }
            break;
        }
    }
    iface->removeInstance(ifInstanceName);
    //setActiveWlanIfaceNameProperty(getFirstActiveWlanIfaceName());

    return ndk::ScopedAStatus::ok();
}

std::pair<std::shared_ptr<IWifiNanIface>, ndk::ScopedAStatus> WifiChipRpc::createNanIfaceInternal() {
	return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
}

std::pair<std::vector<std::string>, ndk::ScopedAStatus> WifiChipRpc::getNanIfaceNamesInternal() {
    if (nan_ifaces_.empty()) {
        return {std::vector<std::string>(), ndk::ScopedAStatus::ok()};
    }
    return {getNames(nan_ifaces_), ndk::ScopedAStatus::ok()};
}

std::pair<std::shared_ptr<IWifiNanIface>, ndk::ScopedAStatus> WifiChipRpc::getNanIfaceInternal(
        const std::string& ifname) {
    const auto iface = findUsingName(nan_ifaces_, ifname);
    if (!iface.get()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS)};
    }
    return {iface, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::removeNanIfaceInternal(const std::string& ifname) {
#if 0
    const auto iface = findUsingName(nan_ifaces_, ifname);
    if (!iface.get()) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }
    invalidateAndClear(nan_ifaces_, iface);
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceRemoved(IfaceType::NAN_IFACE, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceAdded callback";
        }
    }
#endif
    return ndk::ScopedAStatus::ok();
}

std::pair<std::shared_ptr<IWifiP2pIface>, ndk::ScopedAStatus> WifiChipRpc::createP2pIfaceInternal() {

      //To do: workaround to pass VTS test cases GetP2pIfaceNames by skipping check support
      //P2P interface in interface combination since WLAN remote control doesn't need
      //P2P role
#if 0
    if (!canCurrentModeSupportConcurrencyTypeWithCurrentTypes(IfaceConcurrencyType::P2P)) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_AVAILABLE)};
    }
#endif
    std::string ifname = getPredefinedP2pIfaceName();
    //To do: send WIFI_CHIP_CREATE_P2P_IFACE_REQ SOME/IP command to remote side
    std::shared_ptr<WifiP2pIfaceRpc> iface =
            ndk::SharedRefBase::make<WifiP2pIfaceRpc>(ifname);
    p2p_ifaces_.push_back(iface);
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceAdded(IfaceType::P2P, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceAdded callback";
        }
    }
    return {iface, ndk::ScopedAStatus::ok()};

}

std::pair<std::vector<std::string>, ndk::ScopedAStatus> WifiChipRpc::getP2pIfaceNamesInternal() {
    if (p2p_ifaces_.empty()) {
        return {std::vector<std::string>(), ndk::ScopedAStatus::ok()};
    }
    return {getNames(p2p_ifaces_), ndk::ScopedAStatus::ok()};
}

std::pair<std::shared_ptr<IWifiP2pIface>, ndk::ScopedAStatus> WifiChipRpc::getP2pIfaceInternal(
        const std::string& ifname) {
    const auto iface = findUsingName(p2p_ifaces_, ifname);
    if (!iface.get()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS)};
    }
    return {iface, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::removeP2pIfaceInternal(const std::string& ifname) {

    const auto iface = findUsingName(p2p_ifaces_, ifname);
    if (!iface.get()) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }
    invalidateAndClear(p2p_ifaces_, iface);
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceRemoved(IfaceType::P2P, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceRemoved callback";
        }
    }

    return ndk::ScopedAStatus::ok();
}

std::pair<std::shared_ptr<IWifiStaIface>, ndk::ScopedAStatus> WifiChipRpc::createStaIfaceInternal() {
    int32_t staInstanceId;
    ndk::ScopedAStatus status;
    CreateStaIfaceCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_CREATE_STA_IFACE_REQ);
    if (rsp && WifiChipParseCreateStaIfaceCfm(rsp->getData(), rsp->getLength(), param)) {
        staInstanceId = param.result;
        status = WifiParam2NdkStatus(param.status);
    }
    LOG(DEBUG) << "createStaIface return InstanceId 0x" << std::hex << staInstanceId;

    if (!status.isOk()) {
        LOG(ERROR) << "Failed to createStaIface ";
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    if (staInstanceId & 0xffff0000) {
        LOG(ERROR) << "RPC created sta iface with invalid instance id " << std::hex << staInstanceId;
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    GetNameCfmStaIfaceParam paramSta;
    string ifname;
    WifiRpcRequest request(chip_id_, staInstanceId);
    rsp = request.SomeipMsgTransportWithReplyMsg(WIFI_STA_IFACE_GET_NAME_REQ);
    if (rsp && WifiStaIfaceParseGetNameCfm(rsp->getData(), rsp->getLength(), paramSta)) {
        ifname = paramSta.result;
        status = WifiParam2NdkStatus(paramSta.status);
    }

    if (!status.isOk()) {
        LOG(ERROR) << "Failed to createStaIface ";
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
    }

    uint16_t staVlanId = ifname == "wlan0" ? kPrimaryStaVlanId : kSecondaryStaVlanId;
    if (!createVlanInf(ifname, staVlanId))
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};

    std::shared_ptr<WifiStaIfaceRpc> iface = WifiStaIfaceRpc::create(ifname, chip_id_, staInstanceId);
    sta_ifaces_.push_back(iface);
    sta_ifaces_map[iface] = staInstanceId;

    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceAdded(IfaceType::STA, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceAdded callback";
        }
    }

    return {iface, ndk::ScopedAStatus::ok()};
}

std::pair<std::vector<std::string>, ndk::ScopedAStatus> WifiChipRpc::getStaIfaceNamesInternal() {
    if (sta_ifaces_.empty()) {
        return {std::vector<std::string>(), ndk::ScopedAStatus::ok()};
    }
    return {getNames(sta_ifaces_), ndk::ScopedAStatus::ok()};
}

std::pair<std::shared_ptr<IWifiStaIface>, ndk::ScopedAStatus> WifiChipRpc::getStaIfaceInternal(
        const std::string& ifname) {
    const auto iface = findUsingName(sta_ifaces_, ifname);
    if (!iface.get()) {
        return {nullptr, createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS)};
    }
    return {iface, ndk::ScopedAStatus::ok()};
}

ndk::ScopedAStatus WifiChipRpc::removeStaIfaceInternal(const std::string& ifname) {
    const auto iface = findUsingName(sta_ifaces_, ifname);
    if (!iface.get()) {
        return createWifiStatus(WifiStatusCode::ERROR_INVALID_ARGS);
    }

    vector<uint8_t> payload;
    if (WifiChipSerializeRemoveStaIfaceReq(ifname, payload)) {
        auto status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_REMOVE_STA_IFACE_REQ, payload);
        if (!status.isOk()) {
            LOG(ERROR) << "Rpc failed to remove sta interface: " << ifname;
        }
    }

    // Invalidate & remove any dependent objects first.
    invalidateAndRemoveDependencies(ifname);
    deleteVlanInf(ifname);
    invalidateAndClear(sta_ifaces_, iface);
    sta_ifaces_map.erase(iface);
    for (const auto& callback : event_cb_handler_.getCallbacks()) {
        if (!callback->onIfaceRemoved(IfaceType::STA, ifname).isOk()) {
            LOG(ERROR) << "Failed to invoke onIfaceRemoved callback";
        }
    }
    //setActiveWlanIfaceNameProperty(getFirstActiveWlanIfaceName());
    return ndk::ScopedAStatus::ok();
}

std::pair<std::shared_ptr<IWifiRttController>, ndk::ScopedAStatus>
WifiChipRpc::createRttControllerInternal(const std::shared_ptr<IWifiStaIface>& bound_iface) {
    LOG(ERROR) << "createRttControllerInternal: Chip cannot support STAs "
                  "(and RTT by extension)";
    return {nullptr, createWifiStatus(WifiStatusCode::ERROR_NOT_SUPPORTED)};

}

std::pair<std::vector<WifiDebugRingBufferStatus>, ndk::ScopedAStatus>
WifiChipRpc::getDebugRingBuffersStatusInternal() {
    GetDebugRingBuffersStatusCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_DEBUG_RING_BUFFERS_STATUS_REQ);
    if (rsp && WifiChipParseGetDebugRingBuffersStatusCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {std::vector<WifiDebugRingBufferStatus>(),
                createWifiStatus(WifiStatusCode::ERROR_NOT_SUPPORTED)};
}

ndk::ScopedAStatus WifiChipRpc::startLoggingToDebugRingBufferInternal(
    const std::string& ring_name, WifiDebugRingBufferVerboseLevel verbose_level,
    uint32_t max_interval_in_sec, uint32_t min_data_size_in_bytes) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeStartLoggingToDebugRingBufferReq(ring_name, verbose_level, 
            max_interval_in_sec, min_data_size_in_bytes, payload))
        status = SomeipMsgTransportWithReplyStatus(
            WIFI_CHIP_START_LOGGING_TO_DEBUG_RING_BUFFER_REQ, payload);

    return status;
}

ndk::ScopedAStatus WifiChipRpc::forceDumpToDebugRingBufferInternal(const std::string& ring_name) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeForceDumpToDebugRingBufferReq(ring_name, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_FORCE_DUMP_TO_DEBUG_RING_BUFFER_REQ, payload);

    return status;
}

ndk::ScopedAStatus WifiChipRpc::flushRingBufferToFileInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_CHIP_FLUSH_RING_BUFFER_TO_FILE_REQ);
}

ndk::ScopedAStatus WifiChipRpc::stopLoggingToDebugRingBufferInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_CHIP_STOP_LOGGING_TO_DEBUG_RING_BUFFER_REQ);
}

std::pair<WifiDebugHostWakeReasonStats, ndk::ScopedAStatus>
WifiChipRpc::getDebugHostWakeReasonStatsInternal() {
    GetDebugHostWakeReasonStatsCfmChipParam param;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_DEBUG_HOST_WAKE_REASON_STATS_REQ);
    if (rsp && WifiChipParseGetDebugHostWakeReasonStatsCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {WifiDebugHostWakeReasonStats{}, createWifiStatus(WifiStatusCode::ERROR_NOT_SUPPORTED)};
}

ndk::ScopedAStatus WifiChipRpc::enableDebugErrorAlertsInternal(bool enable) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeEnableDebugErrorAlertsReq(enable, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_ENABLE_DEBUG_ERROR_ALERTS_REQ, payload);

    return status;
}

ndk::ScopedAStatus WifiChipRpc::selectTxPowerScenarioInternal(IWifiChip::TxPowerScenario scenario) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeSelectTxPowerScenarioReq(scenario, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SELECT_TX_POWER_SCENARIO_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::resetTxPowerScenarioInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_CHIP_RESET_TX_POWER_SCENARIO_REQ);
}

ndk::ScopedAStatus WifiChipRpc::setLatencyModeInternal(IWifiChip::LatencyMode mode) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiChipSerializeSetLatencyModeReq(mode, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_LATENCY_MODE_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::setMultiStaPrimaryConnectionInternal(const std::string& ifname) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;
    if (WifiChipSerializeSetMultiStaPrimaryConnectionReq(ifname, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_MULTI_STA_PRIMARY_CONNECTION_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::setMultiStaUseCaseInternal(IWifiChip::MultiStaUseCase use_case) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeSetMultiStaUseCaseReq(use_case, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_MULTI_STA_USE_CASE_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::setCoexUnsafeChannelsInternal(
        std::vector<IWifiChip::CoexUnsafeChannel> unsafe_channels, int32_t aidl_restrictions) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeSetCoexUnsafeChannelsReq(unsafe_channels, aidl_restrictions, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_COEX_UNSAFE_CHANNELS_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::setCountryCodeInternal(const std::array<uint8_t, 2>& code) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeSetCountryCodeReq(code, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_COUNTRY_CODE_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

std::pair<std::vector<WifiUsableChannel>, ndk::ScopedAStatus> WifiChipRpc::getUsableChannelsInternal(
        WifiBand band, int32_t ifaceModeMask, int32_t filterMask) {
    vector<uint8_t> payload;
    GetUsableChannelsCfmChipParam param;

    if (WifiChipSerializeGetUsableChannelsReq(band, ifaceModeMask, filterMask, payload)) {
        auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_USABLE_CHANNELS_REQ, payload);
        if (rsp && WifiChipParseGetUsableChannelsCfm(rsp->getData(), rsp->getLength(), param))
            return {param.result, WifiParam2NdkStatus(param.status)};
    }

    return {std::vector<WifiUsableChannel>(), createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiChipRpc::setAfcChannelAllowanceInternal(
        const AfcChannelAllowance& afcChannelAllowance) {
    LOG(INFO) << "setAfcChannelAllowance is not yet supported. availableAfcFrequencyInfos size="
              << afcChannelAllowance.availableAfcFrequencyInfos.size()
              << " availableAfcChannelInfos size="
              << afcChannelAllowance.availableAfcChannelInfos.size()
              << " availabilityExpireTimeMs=" << afcChannelAllowance.availabilityExpireTimeMs;
    return createWifiStatus(WifiStatusCode::ERROR_NOT_SUPPORTED);
}

std::pair<std::vector<WifiRadioCombination>, ndk::ScopedAStatus>
WifiChipRpc::getSupportedRadioCombinationsInternal() {
    GetSupportedRadioCombinationsCfmChipParam param;
    std::vector<WifiRadioCombination> aidl_combinations;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_SUPPORTED_RADIO_COMBINATIONS_REQ);
    if (rsp && WifiChipParseGetSupportedRadioCombinationsCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {aidl_combinations, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

std::pair<WifiChipCapabilities, ndk::ScopedAStatus> WifiChipRpc::getWifiChipCapabilitiesInternal() {
    GetWifiChipCapabilitiesCfmChipParam param;
    WifiChipCapabilities aidl_chip_capabilities;
    auto rsp = SomeipMsgTransportWithReplyMsg(WIFI_CHIP_GET_WIFI_CHIP_CAPABILITIES_REQ);
    if (rsp && WifiChipParseGetWifiChipCapabilitiesCfm(rsp->getData(), rsp->getLength(), param))
        return {param.result, WifiParam2NdkStatus(param.status)};

    return {aidl_chip_capabilities, createWifiStatus(WifiStatusCode::ERROR_UNKNOWN)};
}

ndk::ScopedAStatus WifiChipRpc::enableStaChannelForPeerNetworkInternal(
        int32_t channelCategoryEnableFlag) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeEnableStaChannelForPeerNetworkReq(channelCategoryEnableFlag, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_ENABLE_STA_CHANNEL_FOR_PEER_NETWORK_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

ndk::ScopedAStatus WifiChipRpc::triggerSubsystemRestartInternal() {
    return SomeipMsgTransportWithReplyStatus(WIFI_CHIP_TRIGGER_SUBSYSTEM_RESTART_REQ);
}

std::vector<IWifiChip::ChipConcurrencyCombination>
WifiChipRpc::getCurrentModeConcurrencyCombinations() {
    if (!isValidModeId(current_mode_id_)) {
        LOG(ERROR) << "Chip not configured in a mode yet";
        return std::vector<IWifiChip::ChipConcurrencyCombination>();
    }
    for (const auto& mode : modes_) {
        if (mode.id == current_mode_id_) {
            return mode.availableCombinations;
        }
    }
    CHECK(0) << "Expected to find concurrency combinations for current mode!";
    return std::vector<IWifiChip::ChipConcurrencyCombination>();
}

// Returns a map indexed by IfaceConcurrencyType with the number of ifaces currently
// created of the corresponding concurrency type.
std::map<IfaceConcurrencyType, size_t> WifiChipRpc::getCurrentConcurrencyCombination() {
    std::map<IfaceConcurrencyType, size_t> iface_counts;
    uint32_t num_ap = 0;
    uint32_t num_ap_bridged = 0;
    for (const auto& ap_iface : ap_ifaces_) {
        std::string ap_iface_name = ap_iface->getName();
        if (br_ifaces_ap_instances_.count(ap_iface_name) > 0 &&
            br_ifaces_ap_instances_[ap_iface_name].size() > 1) {
            num_ap_bridged++;
        } else {
            num_ap++;
        }
    }
    iface_counts[IfaceConcurrencyType::AP] = num_ap;
    iface_counts[IfaceConcurrencyType::AP_BRIDGED] = num_ap_bridged;
    iface_counts[IfaceConcurrencyType::NAN_IFACE] = nan_ifaces_.size();
    iface_counts[IfaceConcurrencyType::P2P] = p2p_ifaces_.size();
    iface_counts[IfaceConcurrencyType::STA] = sta_ifaces_.size();
    return iface_counts;
}

// This expands the provided concurrency combinations to a more parseable
// form. Returns a vector of available combinations possible with the number
// of each concurrency type in the combination.
// This method is a port of HalDeviceManager.expandConcurrencyCombos() from framework.
std::vector<std::map<IfaceConcurrencyType, size_t>> WifiChipRpc::expandConcurrencyCombinations(
        const IWifiChip::ChipConcurrencyCombination& combination) {
    int32_t num_expanded_combos = 1;
    for (const auto& limit : combination.limits) {
        for (int32_t i = 0; i < limit.maxIfaces; i++) {
            num_expanded_combos *= limit.types.size();
        }
    }

    // Allocate the vector of expanded combos and reset all concurrency type counts to 0
    // in each combo.
    std::vector<std::map<IfaceConcurrencyType, size_t>> expanded_combos;
    expanded_combos.resize(num_expanded_combos);
    for (auto& expanded_combo : expanded_combos) {
        for (const auto type : {IfaceConcurrencyType::AP, IfaceConcurrencyType::AP_BRIDGED,
                                IfaceConcurrencyType::NAN_IFACE, IfaceConcurrencyType::P2P,
                                IfaceConcurrencyType::STA}) {
            expanded_combo[type] = 0;
        }
    }
    int32_t span = num_expanded_combos;
    for (const auto& limit : combination.limits) {
        for (int32_t i = 0; i < limit.maxIfaces; i++) {
            span /= limit.types.size();
            for (int32_t k = 0; k < num_expanded_combos; ++k) {
                const auto iface_type = limit.types[(k / span) % limit.types.size()];
                expanded_combos[k][iface_type]++;
            }
        }
    }
    return expanded_combos;
}

bool WifiChipRpc::canExpandedConcurrencyComboSupportConcurrencyTypeWithCurrentTypes(
        const std::map<IfaceConcurrencyType, size_t>& expanded_combo,
        IfaceConcurrencyType requested_type) {
    const auto current_combo = getCurrentConcurrencyCombination();

    // Check if we have space for 1 more iface of |type| in this combo
    for (const auto type :
         {IfaceConcurrencyType::AP, IfaceConcurrencyType::AP_BRIDGED,
          IfaceConcurrencyType::NAN_IFACE, IfaceConcurrencyType::P2P, IfaceConcurrencyType::STA}) {
        size_t num_ifaces_needed = current_combo.at(type);
        if (type == requested_type) {
            num_ifaces_needed++;
        }
        size_t num_ifaces_allowed = expanded_combo.at(type);
        if (num_ifaces_needed > num_ifaces_allowed) {
            return false;
        }
    }
    return true;
}

// This method does the following:
// a) Enumerate all possible concurrency combos by expanding the current
//    ChipConcurrencyCombination.
// b) Check if the requested concurrency type can be added to the current mode
//    with the concurrency combination that is already active.
bool WifiChipRpc::canCurrentModeSupportConcurrencyTypeWithCurrentTypes(
        IfaceConcurrencyType requested_type) {
    if (!isValidModeId(current_mode_id_)) {
        LOG(ERROR) << "Chip not configured in a mode yet";
        return false;
    }
    const auto combinations = getCurrentModeConcurrencyCombinations();
    for (const auto& combination : combinations) {
        const auto expanded_combos = expandConcurrencyCombinations(combination);
        for (const auto& expanded_combo : expanded_combos) {
            if (canExpandedConcurrencyComboSupportConcurrencyTypeWithCurrentTypes(expanded_combo,
                                                                                  requested_type)) {
                return true;
            }
        }
    }
    return false;
}

// Note: This does not consider concurrency types already active. It only checks if the
// provided expanded concurrency combination can support the requested combo.
bool WifiChipRpc::canExpandedConcurrencyComboSupportConcurrencyCombo(
        const std::map<IfaceConcurrencyType, size_t>& expanded_combo,
        const std::map<IfaceConcurrencyType, size_t>& req_combo) {
    // Check if we have space for 1 more |type| in this combo
    for (const auto type :
         {IfaceConcurrencyType::AP, IfaceConcurrencyType::AP_BRIDGED,
          IfaceConcurrencyType::NAN_IFACE, IfaceConcurrencyType::P2P, IfaceConcurrencyType::STA}) {
        if (req_combo.count(type) == 0) {
            // Concurrency type not in the req_combo.
            continue;
        }
        size_t num_ifaces_needed = req_combo.at(type);
        size_t num_ifaces_allowed = expanded_combo.at(type);
        if (num_ifaces_needed > num_ifaces_allowed) {
            return false;
        }
    }
    return true;
}

// This method does the following:
// a) Enumerate all possible concurrency combos by expanding the current
//    ChipConcurrencyCombination.
// b) Check if the requested concurrency combo can be added to the current mode.
// Note: This does not consider concurrency types already active. It only checks if the
// current mode can support the requested combo.
bool WifiChipRpc::canCurrentModeSupportConcurrencyCombo(
        const std::map<IfaceConcurrencyType, size_t>& req_combo) {
    if (!isValidModeId(current_mode_id_)) {
        LOG(ERROR) << "Chip not configured in a mode yet";
        return false;
    }
    const auto combinations = getCurrentModeConcurrencyCombinations();
    for (const auto& combination : combinations) {
        const auto expanded_combos = expandConcurrencyCombinations(combination);
        for (const auto& expanded_combo : expanded_combos) {
            if (canExpandedConcurrencyComboSupportConcurrencyCombo(expanded_combo, req_combo)) {
                return true;
            }
        }
    }
    return false;
}

// This method does the following:
// a) Enumerate all possible concurrency combos by expanding the current
//    ChipConcurrencyCombination.
// b) Check if the requested concurrency type can be added to the current mode.
bool WifiChipRpc::canCurrentModeSupportConcurrencyType(IfaceConcurrencyType requested_type) {
    // Check if we can support at least 1 of the requested concurrency type.
    std::map<IfaceConcurrencyType, size_t> req_iface_combo;
    req_iface_combo[requested_type] = 1;
    return canCurrentModeSupportConcurrencyCombo(req_iface_combo);
}

bool WifiChipRpc::isValidModeId(int32_t mode_id) {
    for (const auto& mode : modes_) {
        if (mode.id == mode_id) {
            return true;
        }
    }
    return false;
}

bool WifiChipRpc::isStaApConcurrencyAllowedInCurrentMode() {
    // Check if we can support at least 1 STA & 1 AP concurrently.
    std::map<IfaceConcurrencyType, size_t> req_iface_combo;
    req_iface_combo[IfaceConcurrencyType::STA] = 1;
    req_iface_combo[IfaceConcurrencyType::AP] = 1;
    return canCurrentModeSupportConcurrencyCombo(req_iface_combo);
}

bool WifiChipRpc::isDualStaConcurrencyAllowedInCurrentMode() {
    // Check if we can support at least 2 STA concurrently.
    std::map<IfaceConcurrencyType, size_t> req_iface_combo;
    req_iface_combo[IfaceConcurrencyType::STA] = 2;
    return canCurrentModeSupportConcurrencyCombo(req_iface_combo);
}

#if 0
std::string WifiChipRpc::getFirstActiveWlanIfaceName() {
    if (sta_ifaces_.size() > 0) return sta_ifaces_[0]->getName();
    if (ap_ifaces_.size() > 0) {
        // If the first active wlan iface is bridged iface.
        // Return first instance name.
        for (auto const& it : br_ifaces_ap_instances_) {
            if (it.first == ap_ifaces_[0]->getName()) {
                return it.second[0];
            }
        }
        return ap_ifaces_[0]->getName();
    }
    // This could happen if the chip call is made before any STA/AP
    // iface is created. Default to wlan0 for such cases.
    LOG(WARNING) << "No active wlan interfaces in use! Using default";
    return getWlanIfaceNameWithType(IfaceType::STA, 0);
}

// Return the first wlan (wlan0, wlan1 etc.) starting from |start_idx|
// not already in use.
// Note: This doesn't check the actual presence of these interfaces.
std::string WifiChipRpc::allocateApOrStaIfaceName(IfaceType type, uint32_t start_idx) {
    for (unsigned idx = start_idx; idx < kMaxWlanIfaces; idx++) {
        const auto ifname = getWlanIfaceNameWithType(type, idx);
        if (findUsingNameFromBridgedApInstances(ifname)) continue;
        if (findUsingName(ap_ifaces_, ifname)) continue;
        if (findUsingName(sta_ifaces_, ifname)) continue;
        return ifname;
    }
    // This should never happen. We screwed up somewhere if it did.
    CHECK(false) << "All wlan interfaces in use already!";
    return {};
}

uint32_t WifiChipRpc::startIdxOfApIface() {
    if (isDualStaConcurrencyAllowedInCurrentMode()) {
        // When the HAL support dual STAs, AP should start with idx 2.
        return 2;
    } else if (isStaApConcurrencyAllowedInCurrentMode()) {
        //  When the HAL support STA + AP but it doesn't support dual STAs.
        //  AP should start with idx 1.
        return 1;
    }
    // No concurrency support.
    return 0;
}

// AP iface names start with idx 1 for modes supporting
// concurrent STA and not dual AP, else start with idx 0.
std::string WifiChipRpc::allocateApIfaceName() {
    // Check if we have a dedicated iface for AP.
    std::vector<std::string> ifnames = getPredefinedApIfaceNames(true);
    for (auto const& ifname : ifnames) {
        if (findUsingName(ap_ifaces_, ifname)) continue;
        return ifname;
    }
    return allocateApOrStaIfaceName(IfaceType::AP, startIdxOfApIface());
}

std::vector<std::string> WifiChipRpc::allocateBridgedApInstanceNames() {
    // Check if we have a dedicated iface for AP.
    std::vector<std::string> instances = getPredefinedApIfaceNames(true);
    if (instances.size() == 2) {
        return instances;
    } else {
        int num_ifaces_need_to_allocate = 2 - instances.size();
        for (int i = 0; i < num_ifaces_need_to_allocate; i++) {
            std::string instance_name =
                    allocateApOrStaIfaceName(IfaceType::AP, startIdxOfApIface() + i);
            if (!instance_name.empty()) {
                instances.push_back(instance_name);
            }
        }
    }
    return instances;
}

// STA iface names start with idx 0.
// Primary STA iface will always be 0.
std::string WifiChipRpc::allocateStaIfaceName() {
    return allocateApOrStaIfaceName(IfaceType::STA, 0);
}

bool WifiChipRpc::writeRingbufferFilesInternal() {
    if (!removeOldFilesInternal()) {
        LOG(ERROR) << "Error occurred while deleting old tombstone files";
        return false;
    }
    // write ringbuffers to file
    {
        std::unique_lock<std::mutex> lk(lock_t);
        for (auto& item : ringbuffer_map_) {
            Ringbuffer& cur_buffer = item.second;
            if (cur_buffer.getData().empty()) {
                continue;
            }
            const std::string file_path_raw = kTombstoneFolderPath + item.first + "XXXXXXXXXX";
            const int dump_fd = mkstemp(makeCharVec(file_path_raw).data());
            if (dump_fd == -1) {
                PLOG(ERROR) << "create file failed";
                return false;
            }
            unique_fd file_auto_closer(dump_fd);
            for (const auto& cur_block : cur_buffer.getData()) {
                if (cur_block.size() <= 0 || cur_block.size() > kMaxBufferSizeBytes) {
                    PLOG(ERROR) << "Ring buffer: " << item.first
                                << " is corrupted. Invalid block size: " << cur_block.size();
                    break;
                }
                if (write(dump_fd, cur_block.data(), sizeof(cur_block[0]) * cur_block.size()) ==
                    -1) {
                    PLOG(ERROR) << "Error writing to file";
                }
            }
            cur_buffer.clear();
        }
        // unique_lock unlocked here
    }
    return true;
}

std::string WifiChipRpc::getWlanIfaceNameWithType(IfaceType type, unsigned idx) {
#if 0
    std::string ifname;

    // let the legacy hal override the interface name
    legacy_hal::wifi_error err = legacy_hal_.lock()->getSupportedIfaceName((uint32_t)type, ifname);
    if (err == legacy_hal::WIFI_SUCCESS) return ifname;
#endif
    return getWlanIfaceName(idx);
}

bool WifiChipRpc::findUsingNameFromBridgedApInstances(const std::string& name) {
    for (auto const& it : br_ifaces_ap_instances_) {
        if (it.first == name) {
            return true;
        }
        for (auto const& iface : it.second) {
            if (iface == name) {
                return true;
            }
        }
    }
    return false;
}
#endif

void WifiChipRpc::deleteApIface(const std::string& if_name) {
    if (if_name.empty()) return;
    // delete bridged interfaces if any
    for (auto const& it : br_ifaces_ap_instances_) {
        if (it.first == if_name) {
            for (auto const& iface : it.second) {
                deleteVlanInf(iface);
            }
            deleteVlanInf(if_name);
            br_ifaces_ap_instances_.erase(if_name);
            // ifname is bridged AP, return here.
            return;
        }
    }

    deleteVlanInf(if_name);
}

void WifiChipRpc::invalidateAndClearBridgedApAll() {
    for (auto it = br_ifaces_ap_instances_.begin(); it != br_ifaces_ap_instances_.end();) {
        for (auto const& iface : it->second) {
             deleteVlanInf(iface);
        }
        deleteVlanInf(it->first);
        it = br_ifaces_ap_instances_.erase(it);
    }
    br_ifaces_ap_instances_.clear();
}

ndk::ScopedAStatus WifiChipRpc::setMloModeInternal(const WifiChipRpc::ChipMloMode in_mode) {
    vector<uint8_t> payload;
    ndk::ScopedAStatus status;

    if (WifiChipSerializeSetMloModeReq(in_mode, payload))
        status = SomeipMsgTransportWithReplyStatus(WIFI_CHIP_SET_MLO_MODE_REQ, payload);
    else
        status = createWifiStatus(WifiStatusCode::ERROR_UNKNOWN, "Proto message serialize failure");

    return status;
}

}  // namespace wifi
}  // namespace hardware
}  // namespace android
}  // namespace aidl
