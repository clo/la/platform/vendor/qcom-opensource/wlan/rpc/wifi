/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include <unistd.h>
#include <string>
#include <array>
#include <vector>
#include <queue>
#include <map>
#include <utility>

#include <someip_util.h>

#include "interceptor_msg.h"
#include "interceptor_msg_common.h"
#include "netlink_msg_info.h"
#include "nlinterceptor_service_someip_def.h"
#include "InterceptorRelayRpc.h"

#include <libnlinterceptor/libnlinterceptor.h>

using std::string;
using std::array;
using std::vector;
using std::queue;
using std::map;

using  LibInterceptedSocket = ::android::nlinterceptor::InterceptedSocket;

void nlinterceptorRpcInit();

void nlinterceptorRpcDeinit();

bool isConnectionAvailableOverPolling();

void registerRemoteSocket(LibInterceptedSocket socket, std::shared_ptr<::android::nlinterceptor::InterceptorRelayRpc> interceptorRelayRpc);

bool SomeipMsgTransportWithoutReply(uint16_t type, vector<uint8_t>& payload);

std::shared_ptr<qti::hal::rpc::SomeipMessage> SomeipMsgTransportWithReplyMsg(uint16_t type, vector<uint8_t>& payload);
