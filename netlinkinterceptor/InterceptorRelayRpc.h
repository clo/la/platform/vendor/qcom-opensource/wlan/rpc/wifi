/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include <libnl++/Socket.h>
#include <libnl++/Buffer.h>
#include <libnl++/MessageMutator.h>
#include <queue>
#include <vector>
#include <utility>
#include <mutex>
#include <thread>
#include <message_queue.h>
#include <someip_util.h>
#include <libnlinterceptor/libnlinterceptor.h>

namespace android::nlinterceptor {

using LibInterceptedSocket = ::android::nlinterceptor::InterceptedSocket;

class InterceptorRelayRpc {
   public:
    /**
     * Wrapper around the netlink socket and thread which relays messages.
     *
     * \param nlFamily - netlink family to use for the netlink socket.
     * \param clientNlPid - pid of the client netlink socket.
     * \param clientName - name of the client to be used for debugging.
     */
    InterceptorRelayRpc(uint32_t nlFamily, uint32_t clientNlPid,
                     const std::string& clientName);

    /**
     * Stops the relay thread if running and destroys itself.
     */
    ~InterceptorRelayRpc();

    /**
     * Returns the PID of the internal Netlink socket.
     *
     * \return value of PID,
     */
    uint32_t getPid();

    bool start();  //spawns relay thread.

    /**
     * create remote netlink socket and spawns relay thread.
     */
    std::pair<bool, LibInterceptedSocket> CreateSocketRpc();

    /**
     * Subscribes the internal socket to a single Netlink multicast group.
     *
     * \param nlGroup - Netlink group to subscribe to.
     * \returns - true for success, false for failure.
     */
    bool subscribeGroup(uint32_t nlGroup);

    /**
     * Unsubscribes the internal socket from a single Netlink multicast group.
     *
     * \param nlGroup - Netlink group to unsubscribe from.
     * \returns - true for success, false for failure.
     */
    bool unsubscribeGroup(uint32_t nlGroup);

    /**
     * handle netlink message and forward to local client(wificond)
     */
    void handleNlmsgFromRemote(uint8_t* data, size_t length);

   private:
    std::string mClientName;  ///< Name of client (Wificond, for example).
    std::optional<nl::Socket> mNlSocket;
    const uint32_t mClientNlPid = 0;  ///< pid of local client NL socket.
    const uint32_t mNlFamily = 0;
    int32_t mRemoteNlPid = 0;
    int32_t mRemoteNlFamily = 0;

    /**
     * If set to true, the relay thread should be running. Setting this to false
     * stops the relay thread.
     */
    std::atomic_bool mRunning = false;

    bool closeSocketRpc(); //close remote netlink socket whoes pid is mRemoteNlPid.

    bool nlmsg2ProtoAndSend(const nl::Buffer<nlmsghdr>& msg);  //serialize nlmsg to proto type and send as SOME/IP message payload

    /**
     * Reads incoming Netlink messages destined for mNlSocket from client(wificond)
     * then forward to remote target
     */
    void msgFromLocalToRemote();

    /**
     * Reads incoming Netlink messages destined for mNlSocket from remote target
     * then forward to local client(wificond) via API handleNlmsgFromRemote()
     */
    void msgFromRemoteToLocal();

    /**
     * forward netlink message from local client(wificond) to remote target
     */
    std::thread mMsgFromLocalToRemoteThread;

};

}  // namespace android::nlinterceptor
