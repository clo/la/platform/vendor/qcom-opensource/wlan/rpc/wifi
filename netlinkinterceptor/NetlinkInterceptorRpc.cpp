/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "InterceptorRelayRpc.h"
#include "NetlinkInterceptorRpc.h"
#include "nlinterceptor_someip_manage.h"

#include <android-base/logging.h>
#include <libnl++/Socket.h>
#include <thread>

namespace android::nlinterceptor {

NetlinkInterceptorRpc::NetlinkInterceptorRpc() {
    std::thread start(&NetlinkInterceptorRpc::startSomeipThread, this);
    start.detach();
}
NetlinkInterceptorRpc::~NetlinkInterceptorRpc() {
    std::thread stop(&NetlinkInterceptorRpc::stopSomeipThread, this);
    stop.detach();
}

void NetlinkInterceptorRpc::startSomeipThread() {
    nlinterceptorRpcInit();
}

void NetlinkInterceptorRpc::stopSomeipThread() {
    nlinterceptorRpcDeinit();
}

ndk::ScopedAStatus NetlinkInterceptorRpc::createSocket(
    int32_t nlFamilyAidl, int32_t clientNlPidAidl,
    const std::string& clientName, AidlInterceptedSocket* interceptedSocket) {
    auto nlFamily = static_cast<uint32_t>(nlFamilyAidl);
    auto clientNlPid = static_cast<uint32_t>(clientNlPidAidl);
    uint32_t interceptorNlPid = 0;

    std::shared_ptr<InterceptorRelayRpc> interceptor =
        std::make_shared<InterceptorRelayRpc>(nlFamily, clientNlPid, clientName);

    interceptorNlPid = interceptor->getPid();

    if (interceptorNlPid == 0) {
        LOG(ERROR) << "Failed to create a Netlink socket for " << clientName
                   << ", " << nlFamily << ":" << clientNlPid;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    if (mClientMap.find({nlFamily, interceptorNlPid}) != mClientMap.end()) {
        LOG(ERROR) << "A socket with pid " << interceptorNlPid
                   << " already exists!";
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    if (!isConnectionAvailableOverPolling()) {
        LOG(ERROR) << "SOME/IP connection unavailable";
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    LOG(DEBUG) << "NetlinkInterceptorRpc::createSocket: nlFamilyAidl = " << nlFamily
                   << ", " << "clientNlPidAidl = " << clientNlPid
                   << ", " << "clientName = " << clientName
                   << ", " << "interceptorNlPid = " << interceptorNlPid;

    std::pair<bool, LibInterceptedSocket> remoteSocket = interceptor->CreateSocketRpc();
    if (!remoteSocket.first) {
        LOG(ERROR) << "Failed to create a Netlink socket in remote side";
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    registerRemoteSocket(remoteSocket.second, interceptor);

    if (!interceptor->start()) {
        LOG(ERROR) << "Failed to start interceptor thread!";
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    if (!mClientMap
             .emplace(InterceptedSocket(nlFamily, interceptorNlPid), interceptor)
             .second) {
        // If this happens, it is very bad.
        LOG(FATAL) << "Failed to insert interceptor instance with pid "
                   << interceptorNlPid << " into map!";
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    interceptedSocket->nlFamily = nlFamily;
    interceptedSocket->portId = interceptorNlPid;

    return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus NetlinkInterceptorRpc::closeSocket(
    const AidlInterceptedSocket& interceptedSocket) {
    InterceptedSocket sock(interceptedSocket);

    LOG(DEBUG) << "NetlinkInterceptorRpc::closeSocket: nlFamily: " << sock.nlFamily
               << " portId: " << sock.portId;

    auto interceptorIt = mClientMap.find(sock);
    if (interceptorIt == mClientMap.end()) {
        LOG(ERROR) << "closeSocket Failed! No such socket " << sock;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }
    mClientMap.erase(interceptorIt);

    return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus NetlinkInterceptorRpc::subscribeGroup(
    const AidlInterceptedSocket& interceptedSocket, int32_t nlGroupAidl) {
    InterceptedSocket sock(interceptedSocket);
    auto nlGroup = static_cast<uint32_t>(nlGroupAidl);

    LOG(DEBUG) << "NetlinkInterceptorRpc::subscribeGroup: nlFamily:  " << sock.nlFamily
               << " portId: " << sock.portId
               << " nlGroup: " << nlGroup;

    auto interceptorIt = mClientMap.find(sock);
    if (interceptorIt == mClientMap.end()) {
        LOG(ERROR) << "subscribeGroup failed! No such socket " << sock;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    auto& interceptor = interceptorIt->second;
    if (!interceptor->subscribeGroup(nlGroup)) {
        LOG(ERROR) << "Failed to subscribe " << sock << " to " << nlGroup;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    return ndk::ScopedAStatus::ok();
}

ndk::ScopedAStatus NetlinkInterceptorRpc::unsubscribeGroup(
    const AidlInterceptedSocket& interceptedSocket, int32_t nlGroupAidl) {
    InterceptedSocket sock(interceptedSocket);
    auto nlGroup = static_cast<uint32_t>(nlGroupAidl);

    LOG(DEBUG) << "NetlinkInterceptorRpc::unsubscribeGroup: nlFamily:  " << sock.nlFamily
	           << " portId: " << sock.portId 
                   << "nlGroup: " << nlGroup;

    auto interceptorIt = mClientMap.find(sock);
    if (interceptorIt == mClientMap.end()) {
        LOG(ERROR) << "unsubscribeGroup failed! No such socket " << sock;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }

    auto& interceptor = interceptorIt->second;
    if (!interceptor->unsubscribeGroup(nlGroup)) {
        LOG(ERROR) << "Failed to unsubscribe " << sock << " from " << nlGroup;
        return ndk::ScopedAStatus(AStatus_fromStatus(::android::UNKNOWN_ERROR));
    }
    return ndk::ScopedAStatus::ok();
}
}  // namespace android::nlinterceptor
