/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include "interceptor_message_def.h"

#include "someip_common_def.h"

#define INTERCEPTOR_INSTANCE_ID               ((uint16_t) 0x2220)

#define INTERCEPTOR_EVENTGROUP_ID             ((uint16_t) 0xBBB0)

#define INTERCEPTOR_CLIENT_APP_NAME            "nlinterceptor_client"

#define INTERCEPTOR_SEND_NLMSG_REQ             (uint16_t)(INTERCEPTOR_REQ_BASE + 0x0004)

#define INTERCEPTOR_IND_BASE                   ((uint16_t) 0x8000)

#define INTERCEPTOR_ON_RECEIVE_RESPONSE_IND    (uint16_t)(INTERCEPTOR_IND_BASE + 0x0000)

#define INTERCEPTOR_ON_RECEIVE_SUBCRIBLE_IND   (uint16_t)(INTERCEPTOR_IND_BASE + 0x0001)

#define INTERCEPTOR_EVENT_COUNT                 2
