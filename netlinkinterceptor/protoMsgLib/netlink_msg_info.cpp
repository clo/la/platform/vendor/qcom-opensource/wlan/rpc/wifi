/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "netlink_msg_info.h"
#include <cstring>

bool SerializeNlmsgInfo(const NlmsgInfo& msg, vector<uint8_t>& payload)
{
    payload.resize(2 * sizeof(int32_t) + msg.nlmsg.size());
    std::memcpy(payload.data(), &msg.nlFamily, sizeof(int32_t));
    std::memcpy(payload.data() + sizeof(int32_t), &msg.pid, sizeof(int32_t));
    std::memcpy(payload.data() + 2 * sizeof(int32_t), msg.nlmsg.data(), msg.nlmsg.size());
    return true;
}

bool DeserializeNlmsgInfo(uint8_t* data, size_t length, NlmsgInfo& msg)
{
    if (length <= 2 * sizeof(int32_t))
        return false;
    std::memcpy(&msg.nlFamily, data, sizeof(int32_t));
    std::memcpy(&msg.pid, data + sizeof(int32_t), sizeof(int32_t));
    msg.nlmsg.resize(length - 2 * sizeof(int32_t));
    std::memcpy(msg.nlmsg.data(), data + 2 * sizeof(int32_t), msg.nlmsg.size());
    return true;
}
