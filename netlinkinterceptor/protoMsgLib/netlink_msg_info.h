/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#pragma once

#include <stdio.h>
#include <unistd.h>
#include <utility>
#include <vector>

using std::vector;

typedef struct
{
    int32_t nlFamily;
    int32_t pid;
    vector<uint8_t> nlmsg;
} NlmsgInfo;

bool SerializeNlmsgInfo(const NlmsgInfo& msg, vector<uint8_t>& payload);

bool DeserializeNlmsgInfo(uint8_t* data, size_t length, NlmsgInfo& msg);
