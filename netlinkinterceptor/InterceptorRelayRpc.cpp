/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Changes from Qualcomm Innovation Center are provided under the following license:
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "InterceptorRelayRpc.h"
#include "nlinterceptor_someip_manage.h"
#include <interceptor_message_def.h>

#include <utils/Log.h>
#include <libnlinterceptor/libnlinterceptor.h>
#include <android-base/logging.h>
#include <libnl++/printer.h>
#include <poll.h>
#include <libnl++/generic/FamilyTracker.h>
#include <linux/nl80211.h>
#include <time.h>

#include <chrono>

#include "util.h"

namespace android::nlinterceptor {
using namespace std::chrono_literals;

static constexpr std::chrono::milliseconds kPollTimeout = 300ms;
static constexpr bool kSuperVerbose = false;
using AidlInterceptedSocket =
        ::aidl::android::hardware::net::nlinterceptor::InterceptedSocket;

InterceptorRelayRpc::InterceptorRelayRpc(uint32_t nlFamily, uint32_t clientNlPid,
                                   const std::string& clientName)
    : mClientName(clientName),
      mNlSocket(std::make_optional<nl::Socket>(nlFamily, 0, 0)),
      mClientNlPid(clientNlPid),
      mNlFamily(nlFamily) {}

InterceptorRelayRpc::~InterceptorRelayRpc() {
    mRunning = false;
    if (mMsgFromLocalToRemoteThread.joinable())
        mMsgFromLocalToRemoteThread.join();

}

uint32_t InterceptorRelayRpc::getPid() {
    auto pidMaybe = mNlSocket->getPid();
    CHECK(pidMaybe.has_value()) << "Failed to get pid of nl::Socket!";
    return *pidMaybe;
}

bool InterceptorRelayRpc::nlmsg2ProtoAndSend(const nl::Buffer<nlmsghdr>& msg) {
    auto msgRaw = msg.getRaw();
    auto len = msgRaw.len();
    uint8_t buffer[len];
    memcpy(buffer, msgRaw.ptr(), len);
#if 0
    LOG(VERBOSE) << "nlmsg2ProtoAndSend: nlmsg length is " << len;
    for (size_t i = 0; i < len;) {
        if (len > (i + 4)) {
            ALOGD("0x%0x 0x%0x 0x%0x 0x%0x 0x%0x",
                  buffer[i], buffer[i+1], buffer[i+2], buffer[i+3], buffer[i+4]);
            i = i + 5;
         } else {
            ALOGD("0x%0x ", buffer[i]);
            i++;
        }
    }
#endif
    NlmsgInfo nlmsgInfo;
    nlmsgInfo.nlmsg.assign(buffer, buffer + sizeof(buffer) / sizeof(buffer[0]));
    nlmsgInfo.nlFamily = mRemoteNlFamily;
    nlmsgInfo.pid = mRemoteNlPid;
    vector<uint8_t> payload;
    if (SerializeNlmsgInfo(nlmsgInfo, payload)) {
        if (SomeipMsgTransportWithoutReply(INTERCEPTOR_SEND_NLMSG_REQ, payload))
            return true;
    }

     return false;
}

void InterceptorRelayRpc::msgFromLocalToRemote() {
    pollfd fds[] = {
        mNlSocket->preparePoll(POLLIN),
    };
    while (mRunning) {
        if (poll(fds, countof(fds), kPollTimeout.count()) < 0) {
            PLOG(FATAL) << "poll failed";
            return;
        }
        const auto nlsockEvents = fds[0].revents;

        if (isSocketBad(nlsockEvents)) {
            LOG(ERROR) << "Netlink socket is bad";
            mRunning = false;
            return;
        }
        if (!isSocketReadable(nlsockEvents)) continue;

        const auto [msgMaybe, sa] = mNlSocket->receiveFrom();
        if (!msgMaybe.has_value()) {
            LOG(ERROR) << "Failed to receive Netlink data!";
            mRunning = false;
            return;
        }
        const auto msg = *msgMaybe;
        if (!msg.firstOk()) {
            LOG(ERROR) << "Netlink packet is malformed!";
            // Test messages might be empty, this isn't fatal.
            continue;
        }

        if constexpr (kSuperVerbose) {
            LOG(VERBOSE) << "[nlmsg] wificond --> remote";
            LOG(VERBOSE) << "nlmsg content: " << nl::toString(msg, NETLINK_GENERIC, true);
        }

        if (!InterceptorRelayRpc::nlmsg2ProtoAndSend(msg))
            LOG(ERROR) << "failed to send to remote side ";
	}
}

static uint64_t  getBootupTimeNs()
{
    struct timespec bootTime;
    int status = clock_gettime(CLOCK_BOOTTIME, &bootTime);
    if (status != 0)
        return UINT64_MAX;

    uint64_t bootTimeNs = bootTime.tv_sec * 1000000000 + bootTime.tv_nsec;
    return bootTimeNs;
}

static void updateTSF(nl::MessageMutator& msg) {

    static nl::generic::FamilyTracker gFamilyTracker;
    const auto msgMaybe = gFamilyTracker.parseNl80211(msg);
    if (!msgMaybe.has_value()) return;
    const auto& attrs = msgMaybe->attributes;

    //Refer to GetBssTimestamp() in scan_util.cpp to update TSF in nlmsg attribute
    if (attrs.contains(NL80211_ATTR_BSS)) {

        uint64_t bootTimeNs = getBootupTimeNs();

        if constexpr (kSuperVerbose) {
            LOG(VERBOSE) << "update current TSF: " << bootTimeNs;
        }

        const auto bss = attrs.get<nl::Attributes>(NL80211_ATTR_BSS);

        const auto lastSeen = bss.getBuffer(NL80211_BSS_LAST_SEEN_BOOTTIME);
        if (lastSeen.has_value())
            msg.write(*lastSeen, bootTimeNs);

        const auto bssTsf = bss.getBuffer(NL80211_BSS_TSF);
        if (bssTsf.has_value())
            msg.write(*bssTsf, bootTimeNs);

        const auto beaconTsf = bss.getBuffer(NL80211_BSS_BEACON_TSF);
        if (beaconTsf.has_value())
            msg.write(*beaconTsf, bootTimeNs);
    }

}

void  InterceptorRelayRpc::handleNlmsgFromRemote(uint8_t* data, size_t length) {

     // Need to modify BSS TSF attr with local time
      // the time after kernel boot up in remote is different with local one.
      // Framework will compare the BSS TSF in attr with local Clock.getElapsedSinceBootNanos()
      // if BSS TSF in attr is smaller than Clock.getElapsedSinceBootNanos(),
      // scan results will be filtered out.
      nl::MessageMutator nlmsgsPtr(reinterpret_cast<nlmsghdr*>(data + 8), length - 8);
      for (auto nlmsg : nlmsgsPtr) {
           updateTSF(nlmsg);
      }

     nl::Buffer<nlmsghdr> msg(reinterpret_cast<nlmsghdr*>(data + 8), length -8 );

     if constexpr (kSuperVerbose) {
          LOG(VERBOSE) << "[nlmsg]remote --> wificond, length: " << length;
          LOG(VERBOSE) << "nlmsg content: " << nl::toString(msg, NETLINK_GENERIC);
      }

     if (!mNlSocket->send(msg, mClientNlPid)) {
          LOG(ERROR) << "failed to send message to client !";
          mRunning = false;
          return;
      }
}

std::pair<bool, LibInterceptedSocket> InterceptorRelayRpc::CreateSocketRpc() {
    bool result = false;
    vector<uint8_t> payload;
    CreateSocketCfmParam param;
    if (InterceptorSerializeCreateSocketReq(int32_t(mNlFamily), int32_t(mClientNlPid), mClientName, payload)) {
        auto replyMsg = SomeipMsgTransportWithReplyMsg(INTERCEPTOR_CREATE_SOCKET_REQ, payload);
        if (replyMsg && InterceptorParseCreateSocketCfm(replyMsg->getData(), replyMsg->getLength(), param)) {
            if (param.status.status == 0) {
                mRemoteNlPid = param.result.portId;
                mRemoteNlFamily = param.result.nlFamily;
                LOG(DEBUG) << "CreateSocketRpc: open remote socket whose pid is : uint32 " 
                           << uint32_t(mRemoteNlPid) << " nlFamily is : " << mRemoteNlFamily;
                result = true;
             }
         }
    }

    return std::make_pair(result, LibInterceptedSocket(mRemoteNlFamily, mRemoteNlPid));
}

bool InterceptorRelayRpc::closeSocketRpc() {

    vector<uint8_t> payload;
    HalStatusParam param;
    AidlInterceptedSocket socket;
    socket.nlFamily = mRemoteNlFamily;
    socket.portId = mRemoteNlPid;
    if (InterceptorSerializeCloseSocketReq(socket, payload)) {
        auto replyMsg = SomeipMsgTransportWithReplyMsg(INTERCEPTOR_CLOSE_SOCKET_REQ, payload);
        if (replyMsg && NlinterceptorParseHalStatus(replyMsg->getData(), replyMsg->getLength(), param)) {
            if (param.status == 0) {
                LOG(DEBUG) << "closeSocketRpc: close remote socket whoes pid is : uint32 " << uint32_t(mRemoteNlPid) << " nlFamily is : " << mRemoteNlFamily;
                return true;
             }
        }
    }

    return false;
}

bool InterceptorRelayRpc::start() {
    if (mRunning) {
        LOG(DEBUG)
            << "Can't relay messages: InterceptorRelayRpc is already running!";
        return false;
    }
    if (mMsgFromLocalToRemoteThread.joinable()) {
        LOG(ERROR) << "relay thread is already running!";
        return false;
    }
    if (!mNlSocket.has_value()) {
        LOG(ERROR) << "Netlink socket not initialized!";
        return false;
    }

    mRunning = true;
    mMsgFromLocalToRemoteThread = std::thread(&InterceptorRelayRpc::msgFromLocalToRemote, this);

    LOG(DEBUG) << "Relay threads initialized";
    return true;
}

bool InterceptorRelayRpc::subscribeGroup(uint32_t nlGroup) {
    AidlInterceptedSocket socket;
    socket.nlFamily = mRemoteNlFamily;
    socket.portId = mRemoteNlPid;
    vector<uint8_t> payload;
    HalStatusParam param;
    if (InterceptorSerializeSubscribeGroupReq(socket, int32_t(nlGroup), payload)) {
        auto replyMsg = SomeipMsgTransportWithReplyMsg(INTERCEPTOR_SUBSCRIBE_GROUP_REQ, payload);
	if (replyMsg && NlinterceptorParseHalStatus(replyMsg->getData(), replyMsg->getLength(), param)) {
            if (param.status == 0)
                return true;
        }
    }

    return false;
}

bool InterceptorRelayRpc::unsubscribeGroup(uint32_t nlGroup) {
    AidlInterceptedSocket socket;
    socket.nlFamily = mRemoteNlFamily;
    socket.portId = mRemoteNlPid;
    vector<uint8_t> payload;
    HalStatusParam param;
    if (InterceptorSerializeUnsubscribeGroupReq(socket, int32_t(nlGroup), payload)) {
        auto replyMsg = SomeipMsgTransportWithReplyMsg(INTERCEPTOR_UNSUBSCRIBE_GROUP_REQ, payload);
        if (replyMsg && NlinterceptorParseHalStatus(replyMsg->getData(), replyMsg->getLength(), param)) {
            if (param.status == 0)
                return true;
        }
    }

    return false;
}

}  // namespace android::nlinterceptor
