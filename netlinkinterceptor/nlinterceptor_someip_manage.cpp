/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include <android-base/logging.h>
#include <utils/Log.h>
#include <thread>
#include <cutils/properties.h>
#include <someip_client.h>

#include "nlinterceptor_someip_manage.h"

using qti::hal::rpc::Someip;
using qti::hal::rpc::SomeipCallback;
using qti::hal::rpc::SomeipClient;
using qti::hal::rpc::SomeipContext;
using qti::hal::rpc::SomeipMessage;

#define MAX_WAIT_CONNECT_AVAILABLE_SEC 30

static std::shared_ptr<SomeipClient> client;

//map remote netlink socket pid to its nlmsg recevie queue
static map<LibInterceptedSocket, std::shared_ptr<::android::nlinterceptor::InterceptorRelayRpc>> maps;

static std::vector<uint16_t> interceptor_event_ids = {
    INTERCEPTOR_ON_RECEIVE_RESPONSE_IND,
    INTERCEPTOR_ON_RECEIVE_SUBCRIBLE_IND
};

static void handleExtractAsyncMessage(std::shared_ptr<SomeipMessage> message)
{
    auto length = message->getLength();
    auto data = message->getData();
    //message payload construct in below sequence:
    //{int32_t nlFamily, int32_t pid,  std::vector<uint8_t> nlmsg}
    //Note: nlmsg comes from kernel and its length % 4 = 0
    if (length <= 8 || length % 4 != 0 || length > 65536) {
         LOG(ERROR) << "SOME/IP message payload length is invalid  " << length;
         return;
    }

    NlmsgInfo msgInfo;
    std::memcpy(&msgInfo.nlFamily, data, sizeof(uint32_t));
    std::memcpy(&msgInfo.pid, data + sizeof(uint32_t), sizeof(uint32_t));
    LibInterceptedSocket socket(msgInfo.nlFamily, msgInfo.pid);
    auto socketIt = maps.find(socket);
    if (socketIt != maps.end()) {
      auto interceptorRpc = socketIt->second;
      interceptorRpc->handleNlmsgFromRemote(data, length);
    } else {
        ALOGE("%s: not found msg queue for nlSocket(nlfamily：%u, pid: %u)",
               __func__, msgInfo.nlFamily, msgInfo.pid);
    }
}

static void interceptorSomeipMessageCallback(std::shared_ptr<SomeipMessage> message)
{
    if (message && message->isEvent()) {
        handleExtractAsyncMessage(message);
    }
}

static void interceptorConnectionAvailableCallback(uint16_t service_id, uint16_t instance_id, bool available)
{

    ALOGI("interceptor rpc service [%04x:%04x] %s", service_id, instance_id,
          available ? "available" : "unavailable");

    if (available) {
       ALOGI("SOMEIP connection available.");
       property_set("ctl.start", "wificond");
    } else {
       ALOGI("SOMEIP connection unavailable.");
       property_set("ctl.stop", "wificond");
       LOG(FATAL) << "stop wificond, SOMEIP connection lost";
    }
}

void nlinterceptorRpcInit()
{
    if (client) {
        ALOGI("nlinterceptor rpc client already created");
        return;
    }

    SomeipCallback callback(&interceptorConnectionAvailableCallback,
                            &interceptorSomeipMessageCallback);
    SomeipContext context(WIFI_COND_SERVICE_ID, INTERCEPTOR_INSTANCE_ID,
                          INTERCEPTOR_EVENTGROUP_ID, interceptor_event_ids);

    Someip::setup(callback);
    client = std::make_shared<SomeipClient>(INTERCEPTOR_CLIENT_APP_NAME, context);
    if (!client || !client->start(true)) {
        ALOGE("nlinterceptor rpc client start fail");
        client = nullptr;
        Someip::destroy();
    }
}

void nlinterceptorRpcDeinit()
{
    if (client) {
        client->stop();
        client->deinit();
        client.reset();
        Someip::destroy();
    }

    maps.clear();
}

bool isConnectionAvailableOverPolling()
{
    uint32_t total_wait_sec = 0;
    uint32_t wait_interval_sec = 1;
    while (total_wait_sec < (MAX_WAIT_CONNECT_AVAILABLE_SEC + 1)) {
        if (client && client->isServiceAvailable()) {
            ALOGD("SOMEIP connection available after sec %u", total_wait_sec);
            return true;
        }

        sleep(wait_interval_sec);

        total_wait_sec += wait_interval_sec;
    }

    ALOGE("SOMEIP connection timeout after sec %u", MAX_WAIT_CONNECT_AVAILABLE_SEC);
    return false;
}

void registerRemoteSocket(LibInterceptedSocket socket,
       std::shared_ptr<::android::nlinterceptor::InterceptorRelayRpc> interceptorRelayRpc)
{
    //if key of maps is the same, overwrite the value stored in map.
    //remote side will not create socket after already creating two sockets.
    //that means local side will always maintain two remote pid and two queues.
    //once wificond restarts, need to overwrite the nlmsgFromRemoteQueue stored in maps
    //since LibInterceptedSocket(nlFamily, pid) is the same.
    maps[socket] = interceptorRelayRpc;
}

bool SomeipMsgTransportWithoutReply(uint16_t type, vector<uint8_t>& payload)
{
    if (!client)
        return false;

    auto request = std::make_shared<SomeipMessage>(
        SomeipMessage::createRequest(WIFI_COND_SERVICE_ID,
            INTERCEPTOR_INSTANCE_ID, type, payload, true));
    return client->sendMessage(request);
}

std::shared_ptr<SomeipMessage> SomeipMsgTransportWithReplyMsg(uint16_t type, vector<uint8_t>& payload)
{
    if (!client)
        return nullptr;

    auto request = std::make_shared<SomeipMessage>(
        SomeipMessage::createRequest(WIFI_COND_SERVICE_ID,
            INTERCEPTOR_INSTANCE_ID, type, payload, true));
    auto reply = client->sendRequestAndWaitForReply(request);

    if (reply)
        ALOGD("Recv response method: 0x%04x length: %d", reply->getMethodId(), reply->getLength());
    else
        ALOGE("No response for request method: 0x%4x", type);

    return reply;
}
